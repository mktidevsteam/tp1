module.exports = {
  mode: "spa",

  css: [
    { src: '~assets/materialize_mkti.scss', lang: 'scss' }
  ],

  head: {
    title: 'Otorrino',
    meta: [
      { hid: 'viewport', name: 'viewport', content: 'width=device-width, initial-scale=1, minimal-ui, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no, viewport-fit=cover' }
    ],
    script: [
      { src : 'cordova.js'}
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: 'favicon.ico' }
    ]
  },

  loading: false,

  modules: [
    ['@nuxtjs/google-tag-manager', { id: 'GTM-MD83GLR', pageTracking: true }],
  ],

  plugins: [
    '~/plugins/cordova',
    '~/plugins/gtag',
    '~/plugins/MiniBackend',
    '~/plugins/eventBus',
    '~/plugins/filters',
  ],
  router: {
      mode: 'hash'
  },
  env: {
    'backend_dir_dev': "http://localhost/cluster1/nuxt_otorrino/static",
    'backend_dir_gen': ""
  },

  build: {
    publicPath : '/nuxt/'
  },

  generate : {
    dir : 'dist_cordova'
  }

}
