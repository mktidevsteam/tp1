import Vue from 'vue';


Vue.filter('pretty', (value)=>{
  return JSON.stringify((value), null, 2)
});

Vue.filter('sqldate', (timestamp)=>{
  // Split timestamp into [ Y, M, D, h, m, s ]
  var t = timestamp.split(/[- :]/);
  var d = t[2]+"/"+t[1]+"/"+t[0].slice(-2);
  return d;
});

Vue.filter('slugify', (str)=>{
  str = str.replace(/^\s+|\s+$/g, ''); // trim
  str = str.toLowerCase();

  // remove accents, swap ñ for n, etc
  var from = "àáäâèéëêìíïîòóöôùúüûñç·/_,:;";
  var to   = "aaaaeeeeiiiioooouuuunc------";
  for (var i=0, l=from.length ; i<l ; i++) {
      str = str.replace(new RegExp(from.charAt(i), 'g'), to.charAt(i));
  }

  str = str.replace(/[^a-z0-9 -]/g, '') // remove invalid chars
      .replace(/\s+/g, '-') // collapse whitespace and replace by -
      .replace(/-+/g, '-'); // collapse dashes

  return str;
});
