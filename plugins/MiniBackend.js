  import Vue from 'vue';


  const Backend = {

    "onSubmit": function(form_id, callback, validation_callback, progress_callback, is_encrypted, is_json){
      validation_callback = validation_callback || function(){ return true; };
      var is_encrypted = is_encrypted || false;
      var is_json = is_json || true;
      var progress_callback = progress_callback || function(){ return undefined; };


      form_id = this.parseKey(form_id);
      var $Form = document.getElementById(form_id);

      $Form.addEventListener("submit", (e)=>{
        e.preventDefault();

        if( validation_callback() ){
          $Form.style.opacity = "0.3";
          $Form.style.pointerEvents = "none";
          var form = e.target
          var data = new FormData(form);
          var http = new XMLHttpRequest();

          var action = $Form.getAttribute("action");
          if( action.startsWith("/") ){
            var form_action = (this.route + action);
          }else{
            if( action.startsWith(".") ){
              var form_action = action.substring(1);
            }else{
              var form_action = action;
            }
          }

          var altered_callback = function($AForm, err, res){
            callback(err, res);
            $AForm.style.opacity = "1.0";
            $AForm.style.pointerEvents = "auto";
          }.bind(this, $Form);

          http.onreadystatechange =
            this.onReadyStateFunction(http, altered_callback, is_encrypted, is_json);

          http.open(form.method, form_action, true);

          var progress = 0;
          http.upload.onprogress = function (e) {
            if (e.lengthComputable) {
              progress = Math.floor((e.loaded / e.total) * 100);
              progress = Math.max(1, Math.min(progress, 99) );
              progress_callback(progress);
            }
          }
          http.upload.onloadstart = function (e) {
              progress_callback(0);
          }
          http.upload.onloadend = function (e) {
            progress_callback(100);
            $Form.style.opacity = "1.0";
            $Form.style.pointerEvents = "auto";
          }

          http.send(data);
        }else{
          console.log("Algún dato del formulario no es válido.");
        }
      });

    },

    "parseKey": function(key){
      var type = this.get_key_type(key);
      if( type == "tag"){
        return key;
      }else{
        return key.substring(1);
      }
    },
    "get_key_type": function(key){
      var first_char = key.substring(0, 1);
      if( first_char == "#"){
        return "id";
      }else if(first_char == "."){
        return "class";
      }else{
        return "tag";
      }

    },
    "GET": function(url, callback, params, is_encrypted, is_json){
      this.Http.bind(this, 'GET')(url, callback, params, is_encrypted, is_json);
    },
    "POST": function(url, callback, params, is_encrypted, is_json){
      this.Http.bind(this, 'POST')(url, callback, params, is_encrypted, is_json);
    },

    "Http": function(request_type, url, callback, params, is_encrypted, is_json){
      var params = params || {};
      var is_encrypted = is_encrypted || false;
      var is_json = is_json || true;

      var http  = new XMLHttpRequest();
      http.onload = function (e) {
        // console.log('Inside the onload event'); //success
        // console.log(e);
      };

      http.onreadystatechange =
       this.onReadyStateFunction(http, callback, is_encrypted, is_json);

      var uri_vars = '?stamp=' + new Date().getTime();
      var params_string = this.Assoc2Params(params);
      if(request_type == "GET" && params_string){
        uri_vars += ("&" + params_string);
      }
      http.open(request_type, (url + uri_vars) , true);

      http.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
      http.setRequestHeader('X-Requested-With', 'XMLHttpRequest');
      http.setRequestHeader('vanillaAjax', '1.0');
      if(request_type == "GET"){
        http.send();
      }else{
        http.send( params_string );
      }
    },

    "onReadyStateFunction": function(http, callback, is_encrypted, is_json){
      var is_encrypted = is_encrypted || false;
      var is_json = is_json || true;

      var funcion = function(http, callback, is_encrypted, is_json){
        var DONE = 4;
        var OK = 200;
        if (http.readyState === DONE) {
          if (http.status === OK) {
            var error = "";
            var response = http.responseText;
            if(is_json){
              try {
                response = JSON.parse( http.responseText );
                if(is_encrypted){
                  response = this.DecryptRequest(response);
                }
              } catch (e) {
                var error = ("Error backend | "+ e.message + " | " + http.responseText );
                response = {
                  "status": false,
                  "message": error,
                  "data": null
                };
              }
            }

            callback(error, response);
          } else {
            var error = 'Error http: ' + http.status;
            console.log(error);
            callback(error, null);
          }
        }
      };

      return funcion.bind(this, http, callback, is_encrypted, is_json);
    },

    "DecryptRequest": function(encrypted_request_string){
      if("response" in encrypted_request_string ){
        encrypted_request_string = encrypted_request_string["response"];
      }
      return JSON.parse(encrypted_request_string);
    },

    "Assoc2Params": function(Assoc){
      var request = [];
      for( var key in Assoc ){
        var value = Assoc[key];
        request.push( key +"="+ value );
      }
      return request.join("&");
    },

    "getGET": function(key){
      var url_string = window.location.href;
      var url = new URL(url_string);
      var value = url.searchParams.get( key );
      if(value){
        return value;
      }else{
        return undefined;
      }
    },

    "setCookie": function(name, value, days) {
      var expires;
      days = days || 180; //6 month default.
      if (days) {
        var date = new Date();
        date.setTime(date.getTime()+(days*24*60*60*1000));
        expires = "; expires="+date.toGMTString();
      }
      else {
        expires = "";
      }
      var cookie_string = name+"="+value+expires+"; path=/"
      document.cookie = cookie_string;
      return cookie_string;
    },
    "getCookie": function(name) {
      var nameEQ = name + "=";
      var ca = document.cookie.split(';');
      for(var i=0;i < ca.length;i++) {
          var c = ca[i];
          while (c.charAt(0) === ' ') {
              c = c.substring(1,c.length);
          }
          if (c.indexOf(nameEQ) === 0) {
              return c.substring(nameEQ.length,c.length);
          }
      }
      return null;
    },
    "unsetCookie": function(name) {
      this.setCookie(name,"",-1);
    },

    "getUserToken": function(){
      return this.getCookie("_ut");
    },

    "user_token": function(){
      return this.getCookie("_ut");
    },
    "browser_token": function(){
      return this.getCookie("_bt");
    }
  }

  Backend.install = function (Vue) {

    if( process.env.NODE_ENV == "development"){
      Backend.route = process.env["backend_dir_dev"];
    }else{
      Backend.route = process.env["backend_dir_gen"];
    }

    Vue.prototype.$backend = Backend;
  }

  Vue.use( Backend );
