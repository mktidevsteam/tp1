import Vue from 'vue';
import vueSmoothScroll from 'vue-smooth-scroll'

const eventBus = {};

eventBus.install = function (Vue) {

  if( process.env.NODE_ENV == "development"){
    Vue.prototype.api = process.env["backend_dir_dev"];
  }else{
    Vue.prototype.api = process.env["backend_dir_gen"];
  }

  Vue.prototype.$bus = new Vue();

  Vue.prototype.is_image = function(file_input_type){
    var file_formats = [
      "image/png",
      "image/jpeg",
      "image/gif"
    ];
    return file_formats.includes(file_input_type);
  };

  Vue.prototype.$res2data = function(res, $this){
    for( var key in res.data ){
      if( key in $this ){
        $this[key] = res.data[key];
      }
    }
  };

  Vue.prototype.$validateImage = function( Component, Image ){
    Image = Image || Component.Image;

    if( Image != null ){
      if( ! Component.is_image(Image.type) ){
        Component.setError("El archivo debe ser una imagen jpeg o png.");
      }
      var mismb = process.env.max_image_size_mb;
      if( Image.size > mismb){
        Component.setError("El archivo debe ser menor a "+mismb+"MB");
      }
    }

  };

  Vue.prototype.$today = function(){
    var now = new Date();

    var year = now.getFullYear();
    var month = (now.getMonth() + 1);
    var day = now.getDate();

    if (month < 10) month = "0" + month;
    if (day < 10) day = "0" + day;

    return (year + '-' + month + '-' + day);
  };

  Vue.prototype.$onSubmitSuccess = function( Component, push_route, success_message ){
    success_message = success_message || "¡Listo!";
    push_route = push_route || "/";

    var validation_function = Component.validateForm || function(){
      return this.status();
    };

    var progress_function = progress_function || function(progress_stream){ this.progress = progress_stream };

    Component.$backend.onSubmit( Component.form_id, (err,res)=>{
      Component.loading = false;
      if(err){
        Component.setError(err);
      }else{
        if(res.status){
          Component.setSuccess( success_message );
          if(push_route) Component.$router.push( push_route );
        }else{
          Component.setError(res.message);
        }
      }
    }, validation_function.bind(Component), progress_function.bind(Component) );

  };


  Vue.prototype.$getRoute = function(idx=0){
    // return this.$route.name;
    return this.$route.name.split("-")[idx];
  };

  Vue.prototype.$slugify = function(str){
    str = str.replace(/^\s+|\s+$/g, ''); // trim
    str = str.toLowerCase();

    // remove accents, swap ñ for n, etc
    var from = "àáäâèéëêìíïîòóöôùúüûñç·/_,:;";
    var to   = "aaaaeeeeiiiioooouuuunc------";
    for (var i=0, l=from.length ; i<l ; i++) {
        str = str.replace(new RegExp(from.charAt(i), 'g'), to.charAt(i));
    }

    str = str.replace(/[^a-z0-9 -]/g, '') // remove invalid chars
        .replace(/\s+/g, '-') // collapse whitespace and replace by -
        .replace(/-+/g, '-'); // collapse dashes

    return str;
  };

};

Vue.use(eventBus);
Vue.use(vueSmoothScroll)

// EXAMPLE VUE SCROLL
// <a href="#div-id" v-smooth-scroll="{ duration: 1000, offset: -50 }">Anchor</a>
// <div id="div-id"></div>
