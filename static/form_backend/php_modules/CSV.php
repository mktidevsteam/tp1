<?php
define("DEFAULT_CSV_NAME", "data.csv");
define("CSV_DELIMITER",",");

class CSV extends AbstractStatusObject{

  public function Load($path=DEFAULT_CSV_NAME, $has_headers=true ){
    $Sheet = array_map('str_getcsv', file($path , FILE_IGNORE_NEW_LINES | FILE_SKIP_EMPTY_LINES) );

    if($has_headers){
      $keys = array_shift($Sheet);
      $Table = [];
      foreach($Sheet as $Row){
        $idx = 0;
        $newRow = array();
        foreach($keys as $key){
          $newRow[$key] = $Row[$idx];
          $idx++;
        }
        $Table[] = $newRow;
      }
      return $Table;
    }else{
      return $Sheet;
    }

  }
  public function Delete($path=DEFAULT_CSV_NAME){
    if( file_exists($path) ){
      unlink($path);
    }
  }
  public function getHeaders($path=DEFAULT_CSV_NAME){
    $fd = fopen($path, "r");

    $count = 0;
    while ( ! feof ( $fd ) ){
        if ( $count === 1 )break;
        $buffer = fgetcsv ( $fd, 5000 );
        $count++;
    }
    fclose ($fd);
    return $buffer;

  }
  public function Save($Table, $path=DEFAULT_CSV_NAME, $delimiter=CSV_DELIMITER){

    if( sizeof($Table) == 0){
      $this->setError("No hay datos que guardar en csv.");
      return false;
    }

    $File = fopen($path, 'w');

    $keys = array_keys( $Table[0] );
    $this->fputcsv($File, $keys, $delimiter);
    foreach ($Table as $Row){
      $this->fputcsv($File, array_values($Row), $delimiter );
    }

    if( $path != "php://output" ){
      fclose($File);
    }

    return true;
  }
  public function SaveServe($Table, $path=DEFAULT_CSV_NAME, $delimiter=CSV_DELIMITER){
    $this->setCSVHeaders($path);
    return $this->Save($Table, "php://output", $delimiter);
  }

  public function Insert($Data, $path=DEFAULT_CSV_NAME, $delimiter=CSV_DELIMITER ){

    if( is_assoc($Data) ) $Data = [$Data];

    $keys = $this->getHeaders($path);

    $handle = fopen($path, "a");
    foreach($Data as $Row ){
      $newList = [];
      foreach($keys as $key){
        if( has_key($key, $Row) ){
          $newList[] = $Row[$key];
        }else{
          $newList[] = "";
        }
      }
      $this->fputcsv($handle, $newList, $delimiter);
    }

    fclose($handle);
  }

  public function InsertRows($Data, $path=DEFAULT_CSV_NAME, $delimiter=CSV_DELIMITER ){
    $handle = fopen($path, "a");

    if( !is_array($Data[0]) ) $Data = [$Data];

    foreach($Data as $Row ){
      $this->fputcsv($handle, $Row, $delimiter);
    }

    fclose($handle);
  }
  public function InsertRow($Row, $path=DEFAULT_CSV_NAME, $delimiter=CSV_DELIMITER ){
    $handle = fopen($path, "a");

    $this->fputcsv($handle, $Row, $delimiter);

    fclose($handle);
  }

  public function Serve($path=DEFAULT_CSV_NAME, $Table=null, $delimiter=CSV_DELIMITER){
    if( !is_null($Table) ){
      return $this->SaveServe($Table, $path, $delimiter);
    }

    $success = true;
    try {
      $this->setCSVHeaders($path);
      readfile($path);
      exit();
    } catch (\Exception $e) {
      $this->setError( $e->getMessage() );
      $success = false;
    }
    return $success;
  }

  public function setCSVHeaders($path){
    $filename = basename($path);
    header('Content-type: text/csv');
    header('Content-Disposition: attachment; filename="'.$filename.'"');
    header('Pragma: no-cache');
    header('Expires: 0');

    return $filename;
  }

  public function fputcsv($File, $Array, $delimiter=CSV_DELIMITER){
    foreach( $Array as $idx => $value ) $Array[$idx] = utf8_decode($value);
    fputcsv($File, $Array, $delimiter);
  }
}
