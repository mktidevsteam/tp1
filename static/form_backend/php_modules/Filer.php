<?php
define("MAX_IMAGE_SIZE", 2000000); //2MB
define("MAX_IMAGE_SIZE_MB", (MAX_IMAGE_SIZE/1000000)); //2MB

define("UPLOADED", 0);
define("FILE_TOO_BIG", 1);
define("NOT_UPLOADED", 4);


define("IMAGE_FIT_SMALL", 100);
define("IMAGE_FIT_MEDIUM", 400);
define("IMAGE_FIT_LARGE", 1200);
define("IMAGE_FIT_MAX", 3000);

class Filer extends AbstractStatusObject{
  public $size_limit_mb = 2.0;

  public function __construct(){
    $this->extension = NULL;
    $this->error = 0;
  }
  public function getExtension(){
    return $this->extension;
  }
  public function is_image($file_input_type=NULL){
    if( !is_null($file_input_type) ){
      $image_formats = [
        "image/png",
        "image/jpeg",
        "image/gif"
      ];
      return in_array($file_input_type, $image_formats);
    }else{
      return self::is_image_extension($this->extension);
    }

  }
  public static function is_image_extension($extension){
    return self::is_extension_valid($extension, ["jpg","jpeg","png","gif"]);
  }
  public static function is_extension_valid($extension, $validFormatsArray){
    $is_extension = in_array($extension, $validFormatsArray );
    return $is_extension;
  }

  public function has_file($key, $FILES=NULL ){
    if(is_null($FILES)) $FILES = &$_FILES;

    if( has_key($key, $FILES) ){
      $FILE = $this->parseArrayFile( $FILES[$key] );
      return ( $FILE["error"] != NOT_UPLOADED );
    }else{
      return false;
    }
  }
  public function ValidateImageSet($FILES=NULL){
    if(is_null($FILES)) $FILES = &$_FILES;

    $message = "";
    foreach($FILES as $key => $FILE){
      $FILE = $this->parseArrayFile($FILE);
      if( $FILE["error"]==UPLOADED ){
        if( ! $this->is_image($FILE["type"]) ){
          $message .= "El archivo '$key' debe ser una imagen.\n";}
        if( $FILE["size"] > MAX_IMAGE_SIZE){
          $message .= "El archivo '$key' debe pesar menos de ".MAX_IMAGE_SIZE_MB."MB.\n";}
      }else{
        if( $FILE["error"] != NOT_UPLOADED){
          switch ($FILE["error"]) {
            case FILE_TOO_BIG:
              $message .= "El peso de los archivos en '$key' supera el límite establecido en el servidor (ini). Contacta a tu desarrollador.\n";
              break;
            default:
              $message .= "Error al subir archivo '$key'. Código $FILE[error].\n";
          }
        }
      }
    }
    return $message;
  }
  public function UploadImageSet($directory, $id="", $FILES=NULL ){
    if(is_null($FILES)) $FILES = &$_FILES;

    $NamesAssoc = array();
    foreach($FILES as $key => $FILE){
      $FILE = $this->parseArrayFile($FILE);
      $is_good_image = ( $FILE["error"]==UPLOADED & $this->is_image($FILE["type"]) );
      if( $is_good_image ){
        $r = ("_".randomString(10));
        $base_name = ("$id"."_$key$r");
        $this->Upload($FILE, $directory, $base_name);
        if( $this->status() ){
          $ext = $this->getExtension();

          $base_name_ext = "$base_name.$ext";
          $NamesAssoc[$key] = $base_name_ext;

          $this->fitImage("$directory/$base_name_ext", "$directory/$base_name_ext", IMAGE_FIT_MAX);
          $this->fitImage("$directory/$base_name_ext", "$directory/s_$base_name_ext", IMAGE_FIT_SMALL);
          $this->fitImage("$directory/$base_name_ext", "$directory/m_$base_name_ext", IMAGE_FIT_MEDIUM);
          $this->fitImage("$directory/$base_name_ext", "$directory/l_$base_name_ext", IMAGE_FIT_LARGE);
        }else{
          // error al subir imagen original.
        }
      }
    }

    return $NamesAssoc;
  }
  public function DeleteImageSet($directory, $List){
    foreach($List as $image_name ){
        if( $image_name ){
            foreach( ["","s_","m_","l_"] as $prefix ){
              $path = "$directory$prefix$image_name";
                if( file_exists($path) ){
                  try {
                    unlink($path);
                  } catch (\Exception $e) {
                    // nothing.
                  }
                }
            }
        }
    }
  }

  public function parseArrayFile($FILE){
    if(is_array($FILE["name"])){
      $PARSED = array();
      foreach($FILE as $key => $value){
        $PARSED[$key] = $FILE[$key][0];
      }
      return $PARSED;
    }else{
      return $FILE;
    }
  }
  public function UploadImage($FILE, $directory='', $name=''){
    $basename = basename($FILE["name"]);
    $basenameArray = explode(".", $basename );
    $extension = strtolower( end($basenameArray) );
    if( self::is_image_extension($extension) ){
      return $this->Upload($FILE, $directory='', $name='');
    }else{
      $this->setError("El archivo debe ser una imagen.");
      return NULL;
    }
  }
  public function Upload($FILE, $directory='', $name='' ){
    $FILE = $this->parseArrayFile($FILE);

    if($name==''){ $name = randomString(80); }

    $basename = basename($FILE["name"]);
    $basenameArray = explode(".", $basename );

    $this->name = $FILE["name"];
    $this->type = $FILE["type"];
    $this->extension = strtolower( end($basenameArray) );
    if($this->extension=="jpeg") $this->extension = "jpg";
    $this->original_name = strtolower( $basenameArray[0] );
    $this->tmp_name = $FILE["tmp_name"];
    $this->error = $FILE["error"];
    $this->size = ($FILE["size"] / 1000000);

    $filename = ("$directory$name.$this->extension");

    $no_upload_error = ($this->error == 0);
    if( $no_upload_error ){
      if ( $this->size <= $this->size_limit_mb ) {
        try {
          $uploaded = move_uploaded_file( $this->tmp_name, $filename);
          if( $uploaded ) {
            $this->clearErrors();
            return "$name.$this->extension";
          } else {
            $this->setError("Error al subir el archivo, inténtalo de nuevo.");
            return NULL;
          }
        } catch (Exception $e) {
          $this->setError( "Error al mover archivo dentro del servidor. " . $e->getMessage() );
          return NULL;
        }
      }else{
        $this->setError("El archivo es demasiado grande. Debe pesar menos de $this->size_limit_mb MB");
        return NULL;
      }
    }else{
      $this->setError( $this->getUploadError($this->error) );
      return NULL;
    }

  }

  public function getUploadError($code){
    switch ($code) {
     case UPLOAD_ERR_INI_SIZE:
         $file_error = "El archivo supera el tamaño permitido por el servidor.";break;
     case UPLOAD_ERR_FORM_SIZE:
         $file_error = "El archivo supera el tamaño especificado en el formulario";break;
     case UPLOAD_ERR_PARTIAL:
         $file_error = "El archivo se subió incompleto.";break;
     case UPLOAD_ERR_NO_FILE:
         $file_error = "No hay archivo que sibor";break;
     case UPLOAD_ERR_NO_TMP_DIR:
         $file_error = "No hay directorio temporal para subir archivos.";break;
     case UPLOAD_ERR_CANT_WRITE:
         $file_error = "Error al guardar archivo en el disco.";break;
     case UPLOAD_ERR_EXTENSION:
         $file_error = "Una extensión detuvo la carga del archivo.";break;
     default:
         $file_error = "Error sin identificar.";
    }
    return $file_error;
  }

  public function Arrange($file_post) {
    $file_ary = array();
    $file_count = count($file_post['name']);
    $file_keys = array_keys($file_post);

    for ($i=0; $i<$file_count; $i++) {
        foreach ($file_keys as $key) {
            $file_ary[$i][$key] = $file_post[$key][$i];
        }
    }
    return $file_ary;
  }

  public function fitImage($from_path, $to_path, $size, $quality=74) {
    try {
      $src_image = $this->imageOpen($from_path);
    } catch (Exception $e) {
      $this->setError( "Error al abrir imagen origen. " . $e->getMessage() );
      return NULL;
    }

    $src_width = imagesx($src_image);
    $src_height = imagesy($src_image);

    $ratio = ($src_width / $src_height);
    if( $ratio > 1.0 ){
        // Imagen Horizontal
        if( $size > $src_width){
          $new_width = $src_width;
          $new_height = $src_height;
        }else{
          $new_width = $size;
          $new_height = floor($new_width / $ratio);
        }
      }else{
        // Imagen Vertical
        if( $size > $src_height){
          $new_height = $src_height;
          $new_width = $src_width;
        }else{
          $new_height = $size;
          $new_width = floor($new_height * $ratio);
        }
    }

    try {
      $dst_image = imagecreatetruecolor($new_width, $new_height);
      imagefill($dst_image, 0, 0, imagecolorallocate($dst_image, 255, 255, 255));
      $dst_width = imagesx($dst_image);
      $dst_height = imagesy($dst_image);
    } catch (exception $e) {
      $this->setError("Error al procesar imagen comprimida. " . $e->getMessage());
      return NULL;
    }

    try {
      imagecopyresampled($dst_image, $src_image , $new_x=0, $new_y=0, 0, 0, $new_width, $new_height, $src_width, $src_height);
    } catch (Exception $e) {
      $this->setError( "Error al generar imagen comprimida. " . $e->getMessage() );
      return NULL;
    }

    try {
      $this->imageSave( $dst_image, $to_path, $quality );
    } catch (Exception $e) {
      $this->setError("Error al guardar imagen comprimida. " . $e->getMessage() );
      return NULL;
    }

    return $to_path;

  }
  public function imageOpen($from_path){
    $extension = $this->getExtensionFromString($from_path);

    if($extension=="png"){
      return imagecreatefrompng( $from_path );
    }

    if($extension=="jpeg" or $extension=="jpg"){
      return imagecreatefromjpeg( $from_path );
    }

    if($extension=="gif"){
      return imagecreatefromgif( $from_path );
    }

    throw new Exception("La extensión debe ser de una imagen. Se encontró '.$extension'");
    return NULL;
  }
  public function imageSave($dst_image, $to_path, $quality=74){
    $extension = $this->getExtensionFromString($to_path);

    if($extension=="png"){
      $quality = round($quality/10);
      imagepng($dst_image, $to_path, $quality);
      return true;
    }

    if($extension=="jpeg" or $extension=="jpg"){
      imagejpeg($dst_image, $to_path, $quality);
      return true;
    }

    if($extension=="gif"){
      imagegif($dst_image, $to_path);
      return true;
    }

    throw new Exception("La extensión debe de una imagen. Se encontró '.$extension'");
    return false;
  }
  public function getExtensionFromString($path){
    $array = explode('.', $path);
    return strtolower( end($array) );
  }

}

?>
