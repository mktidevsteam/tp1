<?php
  define("DEFAULT_SELECTION_SIZE", 20);

  class SQLite extends AbstractStatusObject{

    public function __construct(){
      $this->data = NULL;
    }

    // CRUD methods.
    public function Create($table_name, $ValuesTable, $Params=array() ){
      if( is_assoc($ValuesTable) ){
        // Turn assoc into a Table with 1 Row.
        $ValuesTable = [ $ValuesTable ];
      }
      if( ! $this->areValuesMissingInTable($ValuesTable, $Params) ){
        $ValidTable = self::getValidTable($ValuesTable, $Params);
        $comma_separated_fields =
          implode( ", ",
            self::WhitelistArray( array_keys($ValidTable[0]) )
          );

        $table_name = self::WhitelistString($table_name);

        $binds = [];
        $values_query = [];
        foreach( $ValidTable as $idx => $validAssoc ){
          $idx++;
          $bindKeys = [];
          foreach( $validAssoc as $field => $value){
            $bind_key = ":row$idx"."_"."$field";
            $binds[$bind_key] = $value;
            $bindKeys[] = $bind_key;
          }
          $values_query[] = "(".implode(", ",$bindKeys).")";
        }
        $values_query = implode(", ", $values_query);


        $sql_query =
          "INSERT INTO $table_name
           ($comma_separated_fields)
           VALUES $values_query; ";

        $last_inserted_id = $this->Query($sql_query, $binds);
        $this->data = $last_inserted_id;
        return $this->data;
      }else{
        return NULL;
      }
    }
    public function Count($table_name, $Params=array() ){
      $binds = [];
      $table_name = self::WhitelistString($table_name);
      $where_string = $this->getWhereString( $Params, $binds );

      $sql_query =
        "SELECT COUNT(*) AS 'count'
         FROM $table_name
         $where_string;";

      $this->data = $this->Query($sql_query, $binds);

      return $this->data;
    }
    public function Read($table_name, $Params=array() ){
      $binds = [];
      $fields = self::getSelectFields( $Params );
      $table_name = self::WhitelistString($table_name);
      $where_string = $this->getWhereString( $Params, $binds );
      $order_string = $this->getOrderString( $Params );
      $pagination_string = $this->getPaginationString( $Params );

      $sql_query =
        "SELECT $fields
         FROM $table_name
         $where_string
         $order_string
         $pagination_string;";

      $this->data = $this->Query($sql_query, $binds);

      return $this->data;
    }
    public function Update($table_name, $valuesAssoc, $Params=array() ){
      if( ! empty($Params["where"]) ){
        if( ! $this->areValuesMissing($valuesAssoc, $Params) ){
          $validAssoc = self::getValidAssoc($valuesAssoc, $Params);
          $table_name = self::WhitelistString($table_name);

          $binds = [];
          $set_string = self::getSetString($validAssoc, $binds);
          $where_string = $this->getWhereString( $Params, $binds );

          $sql_query =
            "UPDATE $table_name
             SET $set_string
             $where_string;";

          $success = $this->Query($sql_query, $binds);
          $this->data = $success;
          return $this->data;
        }else{
          return false;
        }
      }else{
        $this->data = false;
        $this->setError("El método 'Update' requiere un filtro 'where'.");
        return $this->data;
      }

    }
    public function Delete($table_name, $Params){
      if( ! empty($Params["where"]) ){
        $table_name = self::WhitelistString($table_name);

        $binds = [];
        $where_string = $this->getWhereString( $Params, $binds );

        $sql_query =
          "DELETE FROM $table_name
           $where_string;";

        $success = $this->Query($sql_query, $binds);
        $this->data = $success;
        return $this->data;
      }else{
        $this->data = false;
        $this->setError("El método 'Delete' requiere un filtro 'where'.");
        return $this->data;
      }

    }
    public function Exists($table_name, $Params=array()){
      $Count = $this->Count($table_name, $Params);
      if( ! is_null($Count) ){
        return ($Count[0]["count"]  > 0);
      }else{
        //Error SQL ya declarado.
        return NULL;
      }

    }
    public function API($RequestAssoc){
      $status = false;
      $message = "";
      $data = NULL;
      if( ! key_in("method", $RequestAssoc) ){
        $message = "Error: Falta la propiedad 'method'.";
        return self::vars2assoc($status, $message, $data);
      }
      if( ! key_in("table", $RequestAssoc) ){
        $message = "Error: Falta la propiedad 'table'.";
        return self::vars2assoc($status, $message, $data);
      }

      $method = strtolower( self::WhitelistString($RequestAssoc["method"]) );
      $table = self::WhitelistString( $RequestAssoc["table"] );

      $values = ( key_in("values", $RequestAssoc) ) ? $RequestAssoc["values"] : array();
      $params = ( key_in("params", $RequestAssoc) ) ? $RequestAssoc["params"] : array();

      try {
        switch ($method) {
          case 'create':
            $data = $this->Create($table, $values, $params);
            break;
          case 'count':
            $data = $this->Count($table, $params);
            break;
          case 'read':
            $data = $this->Read($table, $params);
            break;
          case 'update':
            $data = $this->Update($table, $values, $params);
            break;
          case 'delete':
            $data = $this->Delete($table, $params);
            break;
          case 'exists':
            $data = $this->Exists($table, $params);
            break;
          default:
            $message = "Error: El método '$method' no es válido.";
            return self::vars2assoc($status, $message, $data);
        }

        return self::vars2assoc( $this->status(), $this->message(), $data);

      } catch (Exception $e) {
        $message = $e->getMessage();
        return self::vars2assoc($status, $message, $data);
      }
    }

    public function validateSelectionParams($REQUEST=NULL){
      if(is_null($REQUEST)) $REQUEST = &$_REQUEST;

      $message = "";
      if( !has_key("order_field", $REQUEST) ){
        $message .= "Falta el campo de ordenamiento 'order_field'.\n";}

      if( has_key("order_sort", $REQUEST) ){
        if( $REQUEST["order_sort"]!="ASC" & $REQUEST["order_sort"]!="DESC"){
          $message .= "El ordenamiento debe de ser 'ASC' o 'DESC'.\n";
        }
      }

      if( has_key("search_term", $REQUEST) ){
        if( !has_key("search_field", $REQUEST) ){
          $message .= "Falta el campo de búsqueda 'search_field'.\n";
        }
      }

      return $message;
    }
    public function getSelectionParams($Params=array(), $REQUEST=NULL){
      if(is_null($REQUEST)) $REQUEST = &$_REQUEST;

      $from = (has_key("from",$REQUEST)) ? $REQUEST["from"] : 0;
      $count = (has_key("count",$REQUEST)) ? $REQUEST["count"] : DEFAULT_SELECTION_SIZE;
      $order_field = (has_key("order_field", $REQUEST)) ? $REQUEST["order_field"] : "id";
      $order_sort = (has_key("order_sort", $REQUEST)) ? $REQUEST["order_sort"] : "ASC";

      $search_field = (has_key("search_field", $REQUEST)) ? $REQUEST["search_field"] : "id";
      $search_term = (has_key("search_term", $REQUEST)) ? $REQUEST["search_term"] : "";

      $Params["order"] = [$order_field, $order_sort];
      $Params["pagination"] = [
        "from" => $from,
        "count" => $count
      ];

      if($search_term != ""){
        if( !has_key("where", $Params) ){
          $Params["where"] = [];
        }
        $Params["where"][] = ["__operator"=>'%', "$search_field" => $search_term];
      }

      if( has_key("where", $Params) ){
        if( sizeof($Params["where"])==0 ){
          unset($Params["where"]);
        }
      }

      return $Params;
    }

    // CRUD supporting methods.
    private static function getSetString($Assoc, &$binds){
      $set_string = [];
      foreach($Assoc as $field => $value){
        $field = self::WhitelistString($field);
        $bkey = (":$field"."_update");
        $binds[$bkey] = $value;
        $set_string[] = "$field = $bkey";
      }
      $set_string = implode(", ", $set_string);
      return $set_string;
    }
    private function areValuesMissingInTable($ValuesTable, $Params){
      foreach( $ValuesTable as $valuesAssoc){
        if( $this->areValuesMissing($valuesAssoc, $Params) ){
          return true;
        }
      }
      return false;
    }
    private function areValuesMissing($valuesAssoc, $Params){
      if( key_in("fields", $Params) ){
        $fields = $Params["fields"];
        $fields = (is_array($fields)) ? $fields : [$fields];
        $fields = self::WhitelistArray($fields);

        $is_some_value_missing = false;
        foreach($fields as $required_field){
          if( ! key_in($required_field, $valuesAssoc) ){
            $is_some_value_missing  = true;
            $this->setError("Falta un dato: $required_field");
          }
        }

        return $is_some_value_missing;
      }else{
        return false;
      }
    }
    private static function getValidTable($ValuesTable, $Params){
      $ValidTable = [];
      foreach( $ValuesTable as $valuesAssoc){
        $ValidTable[] = self::getValidAssoc($valuesAssoc, $Params);
      }
      return $ValidTable;
    }
    private static function getValidAssoc($valuesAssoc, $Params){
      if( key_in("fields", $Params) ){
        $fields = $Params["fields"];
        $fields = (is_array($fields)) ? $fields : [$fields];
        $fields = self::WhitelistArray($fields);

        $validAssoc = self::maskAssoc($valuesAssoc, $fields);
        return $validAssoc;
      }else{
        $validAssoc = self::WhitelistAssoc($valuesAssoc);
        return $validAssoc;
      }
    }
    private static function getSelectFields($Params){
      if( key_in('fields', $Params) ){
        $fields = $Params['fields'];
        $fields = ( is_array($fields) ) ? $fields : [$fields];
        foreach( $fields as $idx => $value){
          $fields[$idx] = self::WhitelistString($value);
        }
        return implode(', ', $fields );
      }else{
        return '*';
      }
    }
    public function getWhereString( $Params, &$binds ){
      if( key_in('where', $Params) ){
        $Params['where'] = (is_assoc($Params['where']))
          ? [ $Params['where'] ]
          : $Params['where'];

        $where_string = [];
        foreach( $Params['where'] as $WhereAssoc ){
          $where_string[] = $this->parseWhereAssoc( $WhereAssoc, $binds );
        }
        $where_string = ("WHERE " . implode(" AND ", $where_string) ) ;

        return $where_string;
      }else{
        return '';
      }
    }
    private function getPaginationString( $Params ){
      if( key_in('pagination', $Params) ){
        $Pagination = $Params["pagination"];
        $from = (int) $Pagination["from"];
        $count = (int) $Pagination["count"];
        return "LIMIT $from, $count";
      }else{
        return '';
      }
    }
    private function getOrderString($Params){
      if( key_in('order', $Params) ){
        $Orders = $Params["order"];
        $Orders = (count($Orders, COUNT_RECURSIVE) <= 2) ? [$Orders] : $Orders;
        $order_string = [];
        foreach($Orders as $Order){
          $field = self::WhitelistString( $Order[0] );
          $ord = strtoupper( $Order[1] );
          $ord = ($ord == "ASC" or $ord == "DESC") ? $ord : "ASC";
          $order_string[] = "$field $ord";
        }
        $order_string = ("ORDER BY " . implode(", ", $order_string));
        return $order_string;
      }else{
        return '';
      }
    }
    public function parseWhereAssoc( $WhereAssoc, &$binds ){
      if( key_in('__operator', $WhereAssoc) ){
        $operator =  $this->whitelist_operator( $WhereAssoc["__operator"] );
        unset( $WhereAssoc["__operator"] );
      }else{
        $operator = '=';
      }

      $bind_count = (sizeof($binds) + 1);
      $where_string = [];
      foreach($WhereAssoc as $field => $value ){
        $field = self::WhitelistString($field);
        $bkey = ":$field"."_$bind_count"; //binded field name.
        $binds[$bkey] = $value;
        if( $operator != "%" ){
          $where_string[] = "$field $operator $bkey";
        }else{
          $where_string[] = "$field LIKE CONCAT('%',$bkey,'%')";
        }

      }
      $where_string = implode(" AND ", $where_string);
      return $where_string;
    }
    private function whitelist_operator($operator){
      $validOperators = ['=','>','<','>=','<=','%'];
      if( key_in($operator, $validOperators) ){
        return $operator;
      }else{
        $this->setError("El operador '$operator' no es válido, fue reemplazado por '='.");
        return '=';
      }
    }

    // Connection methods.
    public function Connect2SQLite($database_path){
      try {
        $this->con = new PDO("sqlite:$database_path;charset=utf8;");
        $this->con->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
      }catch(PDOException $e) {
        $this->setPDOException( $e );
      }
    }
    public function Connect2MySQL($db_name, $db_user="root", $db_password="root", $db_host="localhost"){
      try {
        $this->con = new PDO("mysql:host=$db_host;dbname=$db_name;charset=utf8;", $db_user, $db_password);
        $this->con->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
      }catch(PDOException $e) {
        if(SHOW_ERRORS) echo $e->getMessage();
        $this->setPDOException( $e );
      }
    }

    // Whitelisting methods.
    public static function WhitelistString($str){
      return preg_replace("/[^a-zA-Z0-9_-]/", "", $str);
    }
    public static function WhitelistArray($Array){
      $WhiteArray = [];
      foreach( $Array as $element ){
        $WhiteArray[] = self::WhitelistString($element);
      }
      return $WhiteArray;
    }
    public static function WhitelistAssoc($valuesAssoc){
      $validAssoc = array();
      foreach( $valuesAssoc as $field => $value){
        $clean_field = self::WhitelistString( $field );
        $validAssoc[$clean_field] = $value;
      }
      return $validAssoc;
    }
    public static function maskAssoc($Assoc, $Keys){
      $validAssoc = array();
      foreach( $Assoc as $field => $value ){
        $is_valid_field = key_in($field, $Keys);
        if( $is_valid_field ){
          $validAssoc[$field] = $value;
        }
      }
      return $validAssoc;
    }

    // Quering methods.
    public function Query($sql_query, $binds=[] ){
      try {
        $stmt = $this->con->prepare($sql_query);
        foreach($binds as $bind_key => $bind_value ){
          $stmt->bindValue("$bind_key", $bind_value);
        }
        $stmt->execute();
      }catch(PDOException $e) {
        $this->setPDOException( $e );
        return NULL;
      }

      return $this->ParseStatement($sql_query, $stmt);
    }
    private function ParseStatement($sql_query, $stmt){
      if(       self::is_select_query($sql_query) ){
        return self::ParseStatementTable($stmt);
      }else if( self::is_insert_query($sql_query) ){
        return $this->getLastId($stmt);
      }else if( self::is_update_query($sql_query) ){
        return $this->status();
      }else {
        //Todo lo demás, devuelve status.
        return $this->status();
      }
    }
    private static function ParseStatementTable($stmt){
      $DataTable = [];
      while ($row = $stmt->fetch() ) {
        $newRow = array();
        foreach($row as $key => $value){
          $is_string_key = ( ! is_numeric($key) );
          if( $is_string_key ){
            $newRow[$key] = $value;
          }
        }
        $DataTable[] = $newRow;
      }

      return $DataTable;
    }
    public static function is_select_query($sql_query){
      return (
        self::is_procedure_query($sql_query,"SELECT") ||
        self::is_procedure_query($sql_query,"SHOW") ||
        self::is_procedure_query($sql_query,"DESCRIBE")
      );
    }
    public static function is_insert_query($sql_query){
      return self::is_procedure_query($sql_query,"INSERT");
    }
    public static function is_update_query($sql_query){
      return self::is_procedure_query($sql_query,"UPDATE");
    }
    public static function is_procedure_query($sql_query, $procedure_name){
      $clean_query = strtolower(trim($sql_query));
      $procedure_name = strtolower(trim($procedure_name));
      $procedure_name_len = strlen($procedure_name);
      return ( $procedure_name == substr($clean_query,0,$procedure_name_len) );
    }
    public function getLastId($stmt){
      try {
        $last_id = $this->con->lastInsertId();
      } catch (Exception $e) {
        return NULL;
      }
      return $last_id;
    }

    // Object status getters.
    public function JSON(){
      // Deprecated, user getJSON instead.
      return json_encode( $this->getAssoc() );
    }
    public function getJSON(){
      return json_encode( $this->getAssoc() );
    }
    public function getAssoc(){
      return self::vars2assoc(
        $this->status(),
        $this->message(),
        $this->data
      );
    }
    private static function vars2assoc($status,$message,$data){
      return array(
        "status" => $status,
        "message" => $message,
        "data" => $data
      );
    }

    public function NOW(){
      return $this->QUERY("SELECT NOW() AS 'NOW'")[0]["NOW"];
    }
    
    // SQL Meta methods
    public function describeTable($table_name){
      $table_name = self::WhitelistString($table_name);
      $query = "DESCRIBE $table_name;";

      $response = $this->Query($query);
      if( sizeof($response) > 0 ){
        return $response;
      }else{
        $this->setError("Error al recuper tabla de la base de datos.");
        return NULL;
      }
    }

    public function describeTableArray($TableArray){
      $description = [];
      foreach( $TableArray as $Column){
        $NULL = ($Column["Null"]=="NO") ? "NOT NULL" : "";
        $DEFAULT = ( is_null($Column["Default"]) ) ? "" : "DEFAULT $Column[Default]";
        $description[] = "$Column[Field] $Column[Type] $NULL $DEFAULT";
      }
      return implode(",", $description);
    }

    public function getTables(){
      $query = "SHOW TABLES";
      $response = $this->Query($query);
      if( sizeof($response) > 0 ){
        $Tables = [];
        foreach( $response as $obj ){
          $table_name = array_values($obj)[0];
          $Tables[] = $table_name;
        }
        return $Tables;
      }else{
        $this->setError("Error al recuper tablas de la base de datos.");
        return NULL;
      }
    }
    public function getDatabase(){
      $query = "SELECT DATABASE();";
      $response = $this->Query($query);
      if( sizeof($response) > 0 ){
        return $response[0]["DATABASE()"];
      }else{
        $this->setError("Error al recuper nombre de la base de datos.");
        return NULL;
      }
    }
    // Other methods.
    public function setPDOException($errorObject){
      $this->setError( "Critical database error: " . $errorObject->getMessage() );
    }

  }
?>
