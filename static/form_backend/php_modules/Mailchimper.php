<?php
  // https://usX.api.mailchimp.com/3.0/lists/{list_id}/members/{email_id}/notes/{id}
  //

  class Mailchimper extends AbstractStatusObject{

    public function __construct($apikey, $server="us18"){
      $this->setServer($server);
      $this->setKey($apikey);

      $this->default_list = NULL;
    }

    public function setList($default_list){
      $this->default_list = $default_list;
    }
    public function setServer($server){
      $this->root = "https://$server.api.mailchimp.com/3.0";
    }
    public function setKey($apikey){
      $this->apikey = $apikey;
    }
    public function setCurl($Curl){
      $this->Curl = $Curl;
    }

    public function Query($endpoint, $type="GET", $Params=array()){
      $Params["apikey"] = $this->apikey;
      $endpoint = "$this->root".$endpoint;
      try {
        return json_decode($this->Curl->request($type, $endpoint, $Params), true);
      } catch (Exception $e) {
        $this->setError( $e->getMessage() );
        return NULL;
      }
    }

    public function Lists(){
      $Lists = $this->Query("/lists");

      if( !$this->status() ){ return NULL; }

      if( has_key("lists", $Lists) ){
        $ListsTable = [];
        foreach($Lists["lists"] as $list){
          $stats = $list["stats"];
          $ListsTable[] = [
            "id" => $list["id"],
            "web_id" => $list["web_id"],
            "date_created" => $list["date_created"],
            "name" => $list["name"],
            "list_rating" => $list["list_rating"],
            "subscribe_url_short" => $list["subscribe_url_short"],
            "beamer_address" => $list["beamer_address"],
            "visibility" => $list["visibility"],

            "member_count" => $stats["member_count"],
            "unsubscribe_count" => $stats["unsubscribe_count"],
            "cleaned_count" => $stats["cleaned_count"],
            "member_count_since_send" => $stats["member_count_since_send"],
            "unsubscribe_count_since_send" => $stats["unsubscribe_count_since_send"],
            "cleaned_count_since_send" => $stats["cleaned_count_since_send"],
            "campaign_count" => $stats["campaign_count"],
            "campaign_last_sent" => $stats["campaign_last_sent"],
            "merge_field_count" => $stats["merge_field_count"],
            "avg_sub_rate" => $stats["avg_sub_rate"],
            "avg_unsub_rate" => $stats["avg_unsub_rate"],
            "target_sub_rate" => $stats["target_sub_rate"],
            "open_rate" => $stats["open_rate"],
            "click_rate" => $stats["click_rate"],
            "last_sub_date" => $stats["last_sub_date"],
            "last_unsub_date" => $stats["last_unsub_date"]
          ];
        }
        return $ListsTable;
      }else{
        return $Lists; // NULL
      }
    }

    public function AddSubscriber($email_address, $list_id=NULL, $merge_fields=array() ){
      $Params = [
        "email_address" => $email_address,
        "status" => "subscribed",
        "language" => "es",
        "email_type" => "html" //or "text"
      ];

      if( sizeof($merge_fields)>0 ){
        $Params["merge_fields"] = $merge_fields;
        // "FNAME": "",
        // "LNAME": "",
        // "ADDRESS": "",
        // "PHONE": ""
      }

      if( is_null($list_id) ){
        $list_id = $this->default_list;
      }

      $Response = $this->Query("/lists/$list_id/members", "POST", $Params);

      if( !$this->status() ){ return false; }

      if( has_key("status", $Response) ){
        if( $Response["status"] == "subscribed" ){
          return true; // All good.
        }else{
          // https://developer.mailchimp.com/documentation/mailchimp/guides/error-glossary/
          if( $Response["title"]=="Member Exists" ){
            return true; // Duplicated user is not really and error.
          }else{
            $this->setError( "status: $Response[status], title: $Response[title], detail: $Response[detail]" );
            return false;
          }
        }
      }else{
        $this->setError( "No status returned." );
        return false;
      }

    }

  }

?>
