<?php

  class AbstractStatusObject{
    protected $messages = [];

    public function __construct(){

    }

    public function status(){
      return ( sizeof($this->messages) == 0 );
    }
    public function message($separator="\n"){
      $messages_string = "";
      foreach($this->messages as $mes){
        $messages_string .= ($mes . $separator);
      }
      return $messages_string;
    }
    public function messages(){
      if( sizeof($this->messages) == 0 ){
        return "";
      }
      if( sizeof($this->messages) == 1 ){
        return $this->messages[0];
      }
      if( sizeof($this->messages) > 1 ){
        return $this->messages;
      }
    }
    public function setError($error_message){
      $this->messages[] = $error_message;
    }
    public function clearErrors(){
      $this->messages = [];
    }
    public function getErrorLog(){
      return array(
        "env_uri" => $_SERVER['REQUEST_URI'],
        "env_request" => json_encode( $_REQUEST ),
        "env_cookies" => json_encode( $_COOKIE ),
        "class" => get_class($this),
        "messages" => json_encode($this->messages)
      );
    }
  }

?>
