<?php
  class Templater{
    public function __construct(){
      // echo "Templater is alive.";
    }
    public function Load($file_path){
      return file_get_contents($file_path);
    }
    public function ApplyAndClear($AssocValues, $HTMLTemplate){
      $HTMLTemplate = $this->Apply($AssocValues, $HTMLTemplate);
      return $this->Clear($HTMLTemplate);
    }
    public function ApplyForeach($AssocTable, $Template){
      $HTML = "";
      foreach($AssocTable as $AssocValues){
        $HTML .= $this->ApplyAndClear($AssocValues, $Template);
      }
      return $HTML;
    }
    public function Apply($AssocValues, $HTMLTemplate){

      $PlaceholdersMarks= $this->getPlaceholdersMarks($HTMLTemplate);
      foreach($PlaceholdersMarks as $placeholder){
        $key = $this->getPlaceholderKey($placeholder);
        if( ! empty($AssocValues[$key]) ){
          $value = $AssocValues[$key];
          $HTMLTemplate = str_replace( $placeholder , $value, $HTMLTemplate );
        }
      }
      return $HTMLTemplate;
    }
    public function Clear($HTMLTemplate){
      $PlaceholdersMarks= $this->getPlaceholdersMarks($HTMLTemplate);
      foreach($PlaceholdersMarks as $placeholder){
        $HTMLTemplate = str_replace( $placeholder , '', $HTMLTemplate );
      }
      return $HTMLTemplate;
    }
    public function getPlaceholdersMarks( $HTMLTemplate ){
      preg_match_all( "/{@.*?@}/", $HTMLTemplate, $PlaceholdersArray);
      return $PlaceholdersArray[0];
    }
    public function getPlaceholderKey($placeholder){
      preg_match("#\{@(.*?)\@}#", $placeholder, $match);
      return trim( $match[1] );
    }
  }

?>
