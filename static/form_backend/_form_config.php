<?php

  $leadVariables = ["nombre", "email", "mensaje"];
  $salesEmails = ["contacto@integrify.mx"];

  $from_name = "Integrify";
  $domain_name = "integrify.mx";
  $from_email = "contacto@$domain_name";
  $email_title = "Nuevo lead en la landing page";

  $logo_width = "140px";
  $logo_height = "40px";

  $leads_csv = "leads_ens_156151561558.csv";

  $mailchimp_leads_list = "e08481a436";
