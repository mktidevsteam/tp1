<?php
include_once("_system_includes.php");

//** ROUTES **//
$Router->REQUEST("/submit_form", function($name=null, $email=null, $message=null) use ($Emailer, $Log){
  $salesTeam = ["hola@tp1.com.mx"];
  $r = jheader();
  $r["data"] = $_REQUEST;

  if( is_null($name) ){
    $r["message"] = "* Falta el nombre.";
    recho($r);}
  if( is_null($email) ){
    $r["message"] = "* Falta el email.";
    recho($r);}

  // Guardar Email.

  // Enviar al Equipo de Ventas.
  $html_template = Templater::Load("./sales_email.html");
  $html_message = Templater::ApplyAndClear($_REQUEST, $html_template);
  foreach( $salesTeam as $sales_email ){
    @$Emailer->Send($sales_email, "Nuevo prospecto: $name", $html_message);
  }

  $file = fopen('leads.txt', 'a');
  $_REQUEST["date"] = date("Y-m-d H:i:s");
  fwrite($file, implode("\t", array_values($_REQUEST) ). "\n");
  fclose($file);

  $r["status"] = true;
  $r["message"] = "Formulario enviado con éxito.";
  recho( $r );

});
