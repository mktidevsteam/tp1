<?php

  class Templater{

    public static function Load($html_file){
      return file_get_contents($html_file);
    }

    public static function ApplyForeach($Table, $html_partial_template){
      $html_partial_doc = "";
      foreach($Table as $Values){
        $html_partial_doc .= Templater::ApplyAndClear($Values, $html_partial_template);
      }
      return $html_partial_doc;
    }
    public static function ApplyAndClear($Values, $html_template){
      $html_doc = Templater::Apply($Values, $html_template);
      return Templater::Clear($html_doc);
    }
    public static function Clear($html){
      $Placeholders = Templater::getPlaceholders($html);
      foreach($Placeholders as $ph){
        $html = str_replace( $ph , '', $html );
      }
      return $html;
    }

    public static function Apply($Values, $html_template){
      $Placeholders = Templater::getPlaceholders($html_template);

      foreach($Placeholders as $ph){
        $key = Templater::getKey($ph);
        if( has_key($key, $Values) ){
          $html_template = str_replace( $ph , $Values[$key], $html_template );}
      }

      return $html_template;
    }

    public static function getPlaceholders( $html, $pattern="/{@.*?@}/"){
      preg_match_all($pattern, $html, $Placeholders);
      return $Placeholders[0];
    }
    public static function getKey($placeholder, $pattern="#\{@(.*?)\@}#"){
      preg_match($pattern, $placeholder, $match);
      return trim( $match[1] );
    }
  }

?>
