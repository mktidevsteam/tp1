<?php
  define("DEFAULT_SELECTION_SIZE", 20);
  define("CONFIGS_TABLE", "config_vars");

  class SQLite{
    // CRUD Methods.
    public function Insert($TABLE, $VALUES, $Params=array() ){
      return $this->Create($TABLE, $VALUES, $Params);
    }
    public function Create($TABLE, $VALUES, $Params=array() ){
      $ReferencedInsertValuesBinds = array();
      $SQL_INSERT_QUERY = SQLInsert::GenerateQuery($TABLE, $VALUES, $Params, $ReferencedInsertValuesBinds);
      $INSERTED_ID = $this->Query($SQL_INSERT_QUERY, $ReferencedInsertValuesBinds);
      return $INSERTED_ID;
    }

    public function Update($TABLE, $ValuesAssoc, $Params=array() ){
      if( ! has_key("where", $Params)  ){
        throw new SQLException ( "UPDATE command requieres a WHERE clause." );}

      $ReferencedWhereUpdateBinds = array();
      $SQL_UPDATE_QUERY = SQLUpdate::GenerateQuery($TABLE, $ValuesAssoc, $Params, $ReferencedWhereUpdateBinds);
      $this->Query($SQL_UPDATE_QUERY, $ReferencedWhereUpdateBinds);
    }

    public function Select($TABLE, $Params=array()){
      return $this->Read($TABLE, $Params);
    }
    public function Read($TABLE, $Params=array()){
      $ReferencedWhereBinds = array();
      $SQL_SELECT_QUERY = SQLSelect::GenerateQuery($TABLE, $Params, $ReferencedWhereBinds);
      return $this->Query($SQL_SELECT_QUERY, $ReferencedWhereBinds);
    }
    public function Count($TABLE, $Params=array() ){
      $ReferencedWhereBinds = array();
      $SQL_COUNT_QUERY = SQLSelect::GenerateCountQuery($TABLE, $Params, $ReferencedWhereBinds);
      $Count = $this->Query($SQL_COUNT_QUERY, $ReferencedWhereBinds);

      return $Count[0]["count"];
    }
    public function Exists($TABLE, $Params=array()){
      $count = $this->Count($TABLE, $Params);
      return ($count>0);
    }

    public function Delete($TABLE, $Params){
      if( ! has_key("where", $Params)  ){
        throw new SQLException ( "DELETE command requieres a WHERE clause." );}

      $ReferencedWhereBinds = array();
      $SQL_DELETE_QUERY = SQLDelete::GenerateQuery($TABLE, $Params, $ReferencedWhereBinds);
      $this->Query($SQL_DELETE_QUERY, $ReferencedWhereBinds);
    }

    // Connection methods.
    public function Connect( $Credentials ){
      $Cr = $Credentials;
      $db_type = (has_key("db_type", $Cr)) ? $Cr["db_type"] : "mysql";

      if( $db_type=="mysql" ){
        if( has_key("db_name", $Cr) ){
          $db_name = $Cr["db_name"];
          $db_user = (has_key("db_user", $Cr)) ? $Cr["db_user"] : "root";
          $db_password = (has_key("db_password", $Cr)) ? $Cr["db_password"] : "";
          $db_host = (has_key("db_host", $Cr)) ? $Cr["db_host"] : "localhost";
          $this->Connect2MySQL($db_name,$db_user,$db_password,$db_host);
        }else{
          throw new SQLException ("Database name is missing.");
        }
      }else{
        if( has_key("database_path", $Cr) ){
          $this->Connect2SQLite( $Cr["database_path"] );
        }else{
          throw new SQLException ("Database path is missing.");
        }
      }
    }
    public function Connect2MySQL($db_name, $db_user="root", $db_password="", $db_host="localhost"){
      $this->con = SQLite::createPDOConnection("mysql:host=$db_host;dbname=$db_name;charset=utf8;", $db_user, $db_password);
    }
    public function Connect2SQLite($database_path){
      $this->con = SQLite::createPDOConnection("sqlite:$database_path;charset=utf8;");
    }
    public static function createPDOConnection($PDO_QUERY, $db_user=null, $db_password=""){
      try{
        $is_sql_lite_connection = is_null($db_user);
        if( $is_sql_lite_connection ){
          $Con = new PDO($PDO_QUERY); //SQLite
        }else{
          $Con = new PDO($PDO_QUERY, $db_user, $db_password); // MySQL
        }
        $Con->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
      } catch (PDOException $e) {
        throw new SQLException ("Error when creating connection: " . $e->getMessage() );
      }

      return $Con;
    }

    // Quering methods.
    public function Execute($SQL_QUERY, $binds=array()){
      return $this->Query($SQL_QUERY, $binds);
    }
    public function Query($SQL_QUERY, $binds=array()){
      return SQLExecute::Execute($this->con, $SQL_QUERY, $binds);
    }
    public function getLastId(){
      return SQLExecute::getLastId($this->con);
    }

    // SQL Meta methods
    public function NOW($plus_hours=0){
      if( $plus_hours == 0 ){
        $SQL_NOW_QUERY = "NOW()";
      }else{
        $SQL_NOW_QUERY = "DATE_ADD(NOW(), INTERVAL $plus_hours HOUR)";
      }

      return $this->Query("SELECT $SQL_NOW_QUERY AS 'NOW'")[0]["NOW"];
    }
    public function describeTable($TABLE){
      $TABLE = SQLFilter::WhitelistString($TABLE);
      return $this->Query("DESCRIBE $TABLE;");
    }
    public function describeTableFields($TableArray){
      $tableDescription = [];
      foreach( $TableArray as $Column){
        $IS_NULL_FIELD_QUERY = ($Column["Null"]=="NO") ? "NOT NULL" : "";
        $HAS_DEFAULT_VALUE_QUERY = ( is_null($Column["Default"]) ) ? "" : "DEFAULT $Column[Default]";
        $tableDescription[] = "$Column[Field] $Column[Type] $IS_NULL_FIELD_QUERY $HAS_DEFAULT_VALUE_QUERY";
      }
      return implode(", ", $tableDescription);
    }

    public function getTables(){
      $RawTable = $this->Query("SHOW TABLES");

      $TableNamesList = [];
      foreach( $RawTable as $RawRow ){
        $value_of_the_first_field = array_values($RawRow)[0];
        $table_name = $value_of_the_first_field;
        $TableNamesList[] = $table_name;
      }
      return $TableNamesList;
    }
    public function getDatabase(){
      $response = $this->Query("SELECT DATABASE();");
      if( sizeof($response) > 0 ){
        return $response[0]["DATABASE()"];
      }else{
        throw new SQLException ("No database found.");
      }
    }

    // Other methods.
    public function Config($key, $value=null){
      $return_value = is_null($value);
      if( $return_value ){
        return $this->ConfigRead($key);
      }else{
        return $this->ConfigWrite($key, $value);
      }
    }
    public function ConfigRead($key){
      try {
        $Data = $this->Read( CONFIGS_TABLE, ["where"=>["c_key"=>$key]] );
      } catch (Exception $e) {
        throw new SQLException ( "Error when reading config '$key': " . $e->getMessage() );
      }

      if( sizeof($Data) == 0 ){
        throw new SQLException ( "Config value '$key' was not found." );
      }

      return $Data[0]["c_value"];
    }
    public function ConfigWrite($key, $value){
      $TABLE = CONFIGS_TABLE;

      $SQL_QUERY =
        "INSERT INTO $TABLE (c_key, c_value)
         VALUES (:key, :value)
         ON DUPLICATE KEY UPDATE
          c_value = VALUES(c_value);";

      $binds = [
        ":key" => $key,
        ":value" => $value
      ];

      $this->Query( $SQL_QUERY, $binds );
    }
  }

  class SQLExecute{
    public static function Execute(&$con, $SQL_QUERY, $binds=array()){
      $StatementObj = SQLExecute::ExecuteSQLStatement($con, $SQL_QUERY, $binds);

      // Decide what to return from a succesfull execution.
      if( SQLExecute::isSelectQuery($SQL_QUERY) ){
        return SQLExecute::ParseSQLTable($StatementObj); }

      if( SQLExecute::isInsertQuery($SQL_QUERY) ){
        return SQLExecute::getLastId($con); }

      // Return NULL on everything else.
      return NULL;
    }
    public static function ExecuteSQLStatement(&$con, $SQL_QUERY, $binds){
      try {
        $StatementObj = $con->prepare($SQL_QUERY);
      } catch (Exception $e) {
        throw new SQLException ( "SQL preparation error: " . $e->getMessage() );
      }

      try {
        foreach($binds as $key => $value ){
          $StatementObj->bindValue($key, $value);
        }
      } catch (Exception $e) {
        throw new SQLException ( "SQL binding error: " . $e->getMessage() );
      }

      try {
        $StatementObj->execute();
      } catch (Exception $e) {
        throw new SQLException ( "SQL execution error: " . $e->getMessage() );
      }

      return $StatementObj;
    }

    public static function ParseSQLTable(&$StatementObj){
      $dataTable = [];
      while($SqlRow = $StatementObj->fetch()) {
        $newRow = array();
        foreach($SqlRow as $key => $value){
          $is_string_key = ( ! is_numeric($key) );
          if( $is_string_key ){
            $newRow[$key] = $value;
          }
        }
        $dataTable[] = $newRow;
      }
      return $dataTable;
    }
    public static function getLastId(&$con){
      try {
        $last_id = $con->lastInsertId();
      } catch (Exception $e) {
        throw new SQLException ("Error when retrieving last inserted id: " . $e->getMessage() );
      }
      return $last_id;
    }
    public static function isSelectQuery($sql_query){
      return (
        SQLExecute::isProcedureQuery($sql_query,"SELECT") ||
        SQLExecute::isProcedureQuery($sql_query,"SHOW") ||
        SQLExecute::isProcedureQuery($sql_query,"DESCRIBE")
      );
    }
    public static function isInsertQuery($sql_query){
      return SQLExecute::isProcedureQuery($sql_query,"INSERT");
    }
    public static function isProcedureQuery($sql_query, $procedure_name){
      $clean_query = strtolower(trim($sql_query));
      $procedure_name = strtolower(trim($procedure_name));
      $procedure_name_len = strlen($procedure_name);
      return ( $procedure_name == substr($clean_query, 0, $procedure_name_len) );
    }
  }

  class SQLInsert{
    public static function GenerateQuery($TABLE, $VALUES, $Params=array(), &$ReferencedValueBinds){
      $FIELDS = SQLInsert::prepareInsertFields($Params, $VALUES);
      $VALUES_QUERY = SQLInsert::prepareBindedValuesByReference($ReferencedValueBinds, $VALUES, $FIELDS, $Params );
      $INSERT_OR_REPLACE = SQLInsert::getInsertionType($Params);
      $TABLE = SQLFilter::WhitelistString($TABLE);
      $ON_DUPLICATE = SQLInsert::generateOnDuplicateQuery($Params);

      $SQL_INSERT_QUERY =
        "$INSERT_OR_REPLACE INTO $TABLE
         (".implode(', ',$FIELDS).")
         VALUES $VALUES_QUERY
         $ON_DUPLICATE;";

      return $SQL_INSERT_QUERY;
    }
    public static function prepareInsertFields($Params, $VALUES){
      $VALUES = SQLFilter::Assoc2Table($VALUES);

      if( sizeof($VALUES) == 0 ){
        throw new SQLException ("There are no values for the Insertion.");
      }
      $FIELDS = array_keys( $VALUES[0] );
      if( has_key("fields", $Params) ){
        $FIELDS = $Params["fields"];
      }
      return SQLFilter::WhitelistArray($FIELDS);
    }
    public static function prepareBindedValuesByReference(&$ValueBinds, $VALUES, $FIELDS, $Params ){
      $VALUES = SQLFilter::Assoc2Table($VALUES);
      if( has_key("fields", $Params) ){
        if( SQLFilter::areAnyKeysMissingInTable( $FIELDS, $VALUES ) ){
          throw new SQLException ("Some required fields are missing from the table rows for insertion.");
        }
        $VALUES = SQLFilter::TableByKeys($VALUES, $FIELDS);
      }
      $VALUES = SQLFilter::WhitelistTable($VALUES);
      return SQLInsert::generateValuesQueryAndBindsByReference( $ValueBinds, $VALUES );
    }
    public static function generateValuesQueryAndBindsByReference(&$binds, $Table){
      foreach( $Table as $idx => $Assoc ){
        $bindCodes = [];
        foreach( $Assoc as $field => $value){
          $bind_code = (":$field"."_"."$idx");
          $bindCodes[] = $bind_code;
          $binds[$bind_code] = $value;
        }
        $values_query[] = "(".implode(", ",$bindCodes).")";
      }
      return implode(", ", $values_query );
    }
    public static function getInsertionType($Params){
      $INSERT_OR_REPLACE = "INSERT";
      if( has_key("replace", $Params) ){
        if($Params["replace"] == true){
          $INSERT_OR_REPLACE = "REPLACE";
        }
      }
      return $INSERT_OR_REPLACE;
    }
    public static function generateOnDuplicateQuery( $Params ){
      if( has_key("on_duplicate", $Params) ){
        $on_duplicate_query = [];
        foreach($fields as $key){
          $key = SQLFilter::WhitelistString($key);
          $on_duplicate_query[] = "$key = VALUES($key)";
        }
        return ( "ON DUPLICATE KEY UPDATE " . implode(", ",$on_duplicate_query) );
      }else{
        $ON_DUPLICATE = "";
      }
    }
  }

  class SQLUpdate{
    public static function GenerateQuery($TABLE, $ValuesAssoc, $Params=[], &$binds){
      if( ! has_key("where", $Params) ){
        throw new SQLException ("SQL Update Requires a WHERE query.");
      }
      if( ! has_key("fields", $Params)){
        throw new SQLException ("'fields' parameter is missing in the Update command.");
      }
      if( SQLFilter::areAnyKeysMissingInAssoc( $Params["fields"], $ValuesAssoc) ){
        throw new SQLException ("Some input keys are missing in the Update command.");
      }

      $ValuesAssoc = SQLFilter::WhitelistAssoc($ValuesAssoc, $Params);
      $TABLE = SQLFilter::WhitelistString($TABLE);
      $SET_QUERY = SQLUpdate::getSETQuery($ValuesAssoc, $binds);
      $WHERE_QUERY = SQLWhere::GenerateQuery($Params["where"], $binds );

      $SQL_UPDATE_QUERY =
        "UPDATE $TABLE
         SET $SET_QUERY
         $WHERE_QUERY;";

      return $SQL_UPDATE_QUERY;
    }
    public static function getSETQuery($ValuesAssoc, &$binds){
      $SET_QUERY = [];
      foreach($ValuesAssoc as $field => $value){
        $bind_code = ":update_$field";
        $binds[$bind_code] = $value;
        $SET_QUERY[] = "$field = $bind_code";
      }
      return implode(", ", $SET_QUERY);
    }
  }

  class SQLDelete{
    public static function GenerateQuery($TABLE, $Params, &$binds){
      $TABLE = SQLFilter::WhitelistString($TABLE);
      $WHERE_QUERY = "";
      if( key_in("where", $Params) ){
        $WHERE_QUERY = SQLWhere::GenerateQuery( $Params["where"], $binds );
      }
      return "DELETE FROM $TABLE $WHERE_QUERY;";
    }
  }

  class SQLSelect{
    public static function GenerateQuery($TABLE, $Params=array(), &$binds){
      $FIELDS_QUERY = SQLSelect::getFieldsQuery( $Params );
      $TABLE = SQLFilter::WhitelistString($TABLE);
      $ORDER_BY_QUERY = SQLSelect::getOrderQuery( $Params );
      $PAGINATION_QUERY = SQLSelect::getPaginationQuery( $Params );

      $WHERE_QUERY = "";
      if( key_in("where", $Params) ){
        $WHERE_QUERY = SQLWhere::GenerateQuery( $Params["where"], $binds );
      }

      $SQL_SELECT_QUERY =
        "SELECT $FIELDS_QUERY
         FROM $TABLE
         $WHERE_QUERY
         $ORDER_BY_QUERY
         $PAGINATION_QUERY;";

      return $SQL_SELECT_QUERY;
    }
    public static function GenerateCountQuery($TABLE, $Params=array(), &$binds){
      $TABLE = SQLFilter::WhitelistString($TABLE);

      $WHERE_QUERY = "";
      if( key_in("where", $Params) ){
        $WHERE_QUERY = SQLWhere::GenerateQuery( $Params["where"], $binds );
      }

      $SQL_COUNT_QUERY =
        "SELECT COUNT(*) AS 'count'
         FROM $TABLE
         $WHERE_QUERY;";

      return $SQL_COUNT_QUERY;
    }

    public static function getFieldsQuery($Params){
      if( key_in('fields', $Params) ){
        $FIELDS = SQLFilter::Scalar2Array($Params['fields']);
        $FIELDS = SQLFilter::WhitelistArray($FIELDS);
        return implode(', ', $FIELDS);
      }else{
        return '*';
      }
    }
    public static function getOrderQuery($Params){
      if( key_in('order', $Params) ){
        $Orderings = $Params["order"];

        $is_single_ordering = (count($Orderings, COUNT_RECURSIVE) <= 2);
        if($is_single_ordering){
          $Orderings = [$Orderings]; // Turn Assoc into Table.
        }

        $ORDER_BY_QUERY = SQLSelect::buildOrderQuery($Orderings);
        return $ORDER_BY_QUERY;
      }else{
        return '';
      }
    }
    public static function buildOrderQuery($OrderingsTable){
      $ORDER_BY_QUERY = [];
      foreach($OrderingsTable as $OrderArray){
        if( sizeof($OrderArray) != 2 ){
          throw new SQLException ("Ordering lists must have 2 elements (field and sort).");
        }
        $order_field =  SQLFilter::WhitelistString( $OrderArray[0] );
        $order_sort = SQLSelect::ValidateOrderCommand( $OrderArray[1] );
        $ORDER_BY_QUERY[] = "$order_field $order_sort";
      }

      return ( "ORDER BY " . implode(", ", $ORDER_BY_QUERY) );
    }
    public static function ValidateOrderCommand($order_sort){
      $validOrderings = ["ASC", "DESC"];
      $order_sort = strtoupper( $order_sort );
      if( ! key_in($order_sort, $validOrderings) ){
        throw new SQLException ("'$order_sort' is not a valid ordering command.");
      }
      return $order_sort;
    }
    public static function getPaginationQuery( $Params ){
      if( key_in('pagination', $Params) ){
        if( ! key_in("from", $Params["pagination"]) ){
          throw new SQLException ("'from' value is missing from pagination query.");
        }
        if( ! key_in("count", $Params["pagination"]) ){
          throw new SQLException ("'count' value is missing from pagination query.");
        }

        $from = (int) $Params["pagination"]["from"]; // Cast (int) to protect from sql injection.
        $count = (int) $Params["pagination"]["count"]; // Cast (int) to protect from sql injection.
        return "LIMIT $from, $count";
      }else{
        return '';
      }
    }
  }

  class SQLWhere{
    public static function GenerateQuery($WhereTable, &$binds){
      $WhereTable = SQLFilter::Assoc2Table($WhereTable);
      $WHERE_QUERY = [];
      foreach( $WhereTable as $WhereAssoc ){
        $WHERE_QUERY[] = SQLWhere::Assoc2QueryByReference($WhereAssoc, $binds );
      }
      $WHERE_QUERY = ("WHERE " . implode(" AND ", $WHERE_QUERY) ) ;
      return $WHERE_QUERY;
    }

    public static function Assoc2QueryByReference($WhereAssoc, &$binds){
      $where_operator = SQLWhere::getOperator($WhereAssoc);
      $WhereAssoc = SQLFilter::WhitelistAssoc($WhereAssoc);

      $PARTIAL_WHERE_QUERY = [];
      $where_count = (sizeof(array_keys($binds)) + 1);
      foreach($WhereAssoc as $where_field => $where_value ){

        if( $where_operator != "IN" ){
          // Regular 1-dimension WHERE.
          $bind_code = (":$where_field"."_"."$where_count");
          $binds[$bind_code] = $where_value;
        }else{
          // Exotic n-dimension WHERE IN.
          $bindCodes = [];
          foreach($where_value as $idx => $single_val){
            $single_code = (":$where_field"."_"."$where_count"."_"."$idx");
            $bindCodes[] = $single_code;
            $binds[$single_code] = $single_val;
          }
          $bind_code = implode(", ", $bindCodes);
        }

        $PARTIAL_WHERE_QUERY[] = SQLWhere::buildWhereOperation($where_field, $where_operator, $bind_code);
        $where_count++;
      }

      return implode(" AND ", $PARTIAL_WHERE_QUERY);
    }
    public static function getOperator(&$WhereAssoc){
      if( key_in('__operator', $WhereAssoc) ){
        $operator = SQLWhere::whitelist_operator( $WhereAssoc["__operator"] );
        unset( $WhereAssoc["__operator"] );
        return $operator;
      }else{
        return '=';
      }
    }
    public static function whitelist_operator($operator){
      $validOperators = ['=','>','<','>=','<=','%','IN'];
      if( key_in($operator, $validOperators) ){
        return $operator;
      }else{
        throw new SQLException ("'$operator' is not a valid WHERE operator.");
      }
    }

    public static function buildWhereOperation($where_field, $where_operator, $bind_code){
      switch($where_operator) {
        case '%':
          return "$where_field LIKE CONCAT('%',$bind_code,'%')";
        case 'IN':
          return "$where_field IN($bind_code)";
        default:
          return "$where_field $where_operator $bind_code";
      }
    }
  }

  class SQLFilter {
    public static function Assoc2Table($Element){
      if( is_assoc($Element) ){
        $Element = [ $Element ]; //return Table;
        return $Element;
      }else{
        return $Element; //return same Element, must be a Table.
      }
    }
    public static function Scalar2Array($scalar){
      if( is_array($scalar) ){
        return $scalar;
      }else{
        return [$scalar];
      }
    }

    public static function TableByKeys($Table, $ValidKeys){
      $FilteredTable = [];
      foreach( $Table as $Assoc){
        $FilteredTable[] = SQLFilter::AssocByKeys($Assoc, $ValidKeys);
      }
      return $FilteredTable;
    }
    public static function AssocByKeys($Assoc, $ValidKeys){
      $ValidAssoc = array();
      foreach( $Assoc as $key => $value ){
        $is_valid_key = (key_in($key, $ValidKeys));
        if( $is_valid_key ){
          $ValidAssoc[$key] = $value;
        }
      }
      return $ValidAssoc;
    }

    public static function WhitelistTable($Table){
      $WhitelistedTable = [];
      foreach( $Table as $Assoc){
        $WhitelistedTable[] = SQLFilter::WhitelistAssoc($Assoc);
      }
      return $WhitelistedTable;
    }
    public static function WhitelistAssoc($Assoc){
      $WhitelistedAssoc = array();
      foreach( $Assoc as $key => $value){
        $whitelisted_key = SQLFilter::WhitelistString( $key );
        $WhitelistedAssoc[$whitelisted_key] = $value;
      }
      return $WhitelistedAssoc;
    }
    public static function WhitelistArray($Array){
      $WhitelistedArray = [];
      foreach( $Array as $a ){
        $WhitelistedArray[] = SQLFilter::WhitelistString($a);
      }
      return $WhitelistedArray;
    }
    public static function WhitelistString($string){
      return preg_replace("/[^a-zA-Z0-9_-]/", "", $string);
    }

    public static function areAnyKeysMissingInTable( $Keys, $Table ){
      foreach( $Table as $Assoc ){
        if( SQLFilter::areAnyKeysMissingInAssoc($Keys, $Assoc) ){
          return true;
        }
      }
      return false;
    }
    public static function areAnyKeysMissingInAssoc ($Keys, $Assoc ){
      $Keys = SQLFilter::Scalar2Array($Keys);
      foreach($Keys as $required_key){
        $is_key_missing = (!key_in($required_key, $Assoc));
        if( $is_key_missing ){
          return true; // Some key is missing, stop execution.
        }
      }
      return false; // No key is missing.
    }
  }

  class SQL extends SQLite {}
  class SQLException extends Exception {}
