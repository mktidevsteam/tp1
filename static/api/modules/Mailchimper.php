<?php
  // https://usX.api.mailchimp.com/3.0/lists/{list_id}/members/{email_id}/notes/{id}
  //
  define("MC_API_VERSION",  "3.0");

  class Mailchimper{

    public function __construct($apikey, $server="us18"){
      $this->setServer($server);
      $this->setKey($apikey);

      $this->default_list = NULL;
    }

    public function setServer($server){
      $this->root = ("https://$server.api.mailchimp.com/" . MC_API_VERSION);
    }
    public function setKey($apikey){
      $this->apikey = $apikey;
    }

    public function Execute($endpoint, $method="GET", $Params=array() ){
      $Params["apikey"] = $this->apikey;
      $endpoint = ("$this->root" . $endpoint);


      $Response = Curl::REQUEST($method, $endpoint, $Params);
      return json_decode($Response->body, true);
    }

    public function Lists(){
      $Response = $this->Execute("/lists");
      if( ! has_key("lists", $Response) ){
        $this->HandleApiError($Response);
      }

      $ListsTable = [];
      foreach( $Response["lists"] as $ListAssoc){
        $ListsTable[] = self::ParseListSummary($ListAssoc);
      }

      return $ListsTable;
    }
    public static function ParseListSummary($ListAssoc){
      return [
        "id" => $ListAssoc["id"],
        "web_id" => $ListAssoc["web_id"],
        "date_created" => $ListAssoc["date_created"],
        "name" => $ListAssoc["name"],
        "list_rating" => $ListAssoc["list_rating"],
        "subscribe_url_short" => $ListAssoc["subscribe_url_short"],
        "beamer_address" => $ListAssoc["beamer_address"],
        "visibility" => $ListAssoc["visibility"],

        "member_count" => $ListAssoc["stats"]["member_count"],
        "unsubscribe_count" => $ListAssoc["stats"]["unsubscribe_count"],
        "cleaned_count" => $ListAssoc["stats"]["cleaned_count"],
        "member_count_since_send" => $ListAssoc["stats"]["member_count_since_send"],
        "unsubscribe_count_since_send" => $ListAssoc["stats"]["unsubscribe_count_since_send"],
        "cleaned_count_since_send" => $ListAssoc["stats"]["cleaned_count_since_send"],
        "campaign_count" => $ListAssoc["stats"]["campaign_count"],
        "campaign_last_sent" => $ListAssoc["stats"]["campaign_last_sent"],
        "merge_field_count" => $ListAssoc["stats"]["merge_field_count"],
        "avg_sub_rate" => $ListAssoc["stats"]["avg_sub_rate"],
        "avg_unsub_rate" => $ListAssoc["stats"]["avg_unsub_rate"],
        "target_sub_rate" => $ListAssoc["stats"]["target_sub_rate"],
        "open_rate" => $ListAssoc["stats"]["open_rate"],
        "click_rate" => $ListAssoc["stats"]["click_rate"],
        "last_sub_date" => $ListAssoc["stats"]["last_sub_date"],
        "last_unsub_date" => $ListAssoc["stats"]["last_unsub_date"]
      ];
    }

    public function GetSubscribers($list_id, $count=500, $offset=0){
      $Params = [
        "count" => $count,
        "offset" => $offset
      ];
      $Response = $this->Execute("/lists/$list_id/members", "GET", $Params);
      return $Response;
    }
    public function GetSubscriber($list_id, $email ){
      $subscriber_hash = strtolower( md5($email) );
      $Response = $this->Execute("/lists/$list_id/members/$subscriber_hash", "GET");
      return $Response;
    }
    public function AddSubscriber($list_id, $email, $merge_fields=array() ){
      $Params = [
        "email_address" => $email,
        "status" => "subscribed",
        "language" => "es",
        "email_type" => "html" // or "text"
      ];

      if( has_key("email_address", $merge_fields) ){
        $Params["email_address"] = $merge_fields["email_address"];
        unset($merge_fields["email_address"]);
      }

      if( sizeof($merge_fields) > 0 ){
        $Params["merge_fields"] = $merge_fields;
      }

      $subscriber_hash = strtolower( md5($email) );
      $Response = $this->Execute("/lists/$list_id/members/$subscriber_hash", "PUT", $Params);

      if( ! has_key("status", $Response) ){
        $this->HandleApiError($Response);
      }

      if( $Response["status"] != "subscribed" ){
        if( $Response["title"] != "Member Exists" ){
          throw new MailchimpException( "status: $Response[status], title: $Response[title], detail: $Response[detail]" );
        }
      }

      return $Response;
    }
    public function RemoveSubscriber($list_id, $email ){
      $subscriber_hash = strtolower( md5($email) );
      $Response = $this->Execute("/lists/$list_id/members/$subscriber_hash", "DELETE");
      return $Response;
    }

    public function GetTags($list_id, $email ){
      $subscriber_hash = strtolower( md5($email) );
      $Response = $this->Execute("/lists/$list_id/members/$subscriber_hash/tags", "GET");

      $TagsArray = [];
      if( has_key("tags", $Response) ){
        foreach( $Response["tags"] as $Tag){
          $TagsArray[] = $Tag["name"];
        }
      }
      return $TagsArray;
    }
    public function AddTags($list_id, $email, $TagsArray ){
      $subscriber_hash = strtolower( md5($email) );

      $TagsArray = Scalar2Array($TagsArray);
      $TagsTable = [];
      foreach( $TagsArray as $tag ){
        $TagsTable[] = [
          "name" => $tag,
          "status" => "active"
        ];
      }
      $Response = $this->Execute("/lists/$list_id/members/$subscriber_hash/tags", "POST", ["tags" => $TagsTable] );
      return $Response;
    }
    public function RemoveTags($list_id, $email, $TagsArray ){
      $subscriber_hash = strtolower( md5($email) );

      $TagsArray = Scalar2Array($TagsArray);
      $TagsTable = [];
      foreach( $TagsArray as $tag ){
        $TagsTable[] = [
          "name" => $tag,
          "status" => "inactive"
        ];
      }
      $Response = $this->Execute("/lists/$list_id/members/$subscriber_hash/tags", "POST", ["tags" => $TagsTable] );
      return $Response;
    }

    public function GetNotes($list_id, $email ){
      $subscriber_hash = strtolower( md5($email) );
      $Response = $this->Execute("/lists/$list_id/members/$subscriber_hash/notes", "GET" );

      $NotesTable = [];
      if( has_key("notes", $Response) ){
        foreach($Response["notes"] as $Note ){
          $NotesTable[] = [
            "note_id" => $Note["id"],
            "list_id" => $Note["list_id"],
            "note_created_at" => $Note["created_at"],
            "note_message" => $Note["note"],
          ];
        }
      }
      return $NotesTable;
    }
    public function AddNote($list_id, $email, $note_message ){
      $subscriber_hash = strtolower( md5($email) );
      $Response = $this->Execute("/lists/$list_id/members/$subscriber_hash/notes", "POST", ["note" => $note_message] );
      return $Response;
    }
    public function UpdateNote($list_id, $email, $note_id, $note_message ){
      $subscriber_hash = strtolower( md5($email) );
      $Response = $this->Execute("/lists/$list_id/members/$subscriber_hash/notes/$note_id", "PATCH", ["note" => $note_message] );
      return $Response;
    }
    public function DeleteNote($list_id, $email, $note_id ){
      $subscriber_hash = strtolower( md5($email) );
      $Response = $this->Execute("/lists/$list_id/members/$subscriber_hash/notes/$note_id", "DELETE");
      return $Response;
    }

    public function HandleApiError($Response){
      // https://developer.mailchimp.com/documentation/mailchimp/guides/error-glossary/
      $encoded_response = json_encode($Response);
      throw new MailchimpException ( "Mailchimp API error: $encoded_response.");
    }

  }

  class MailchimpException extends Exception {}

?>
