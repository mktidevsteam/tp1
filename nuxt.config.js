// const pkg = require('./package')

module.exports = {
  mode: "universal",

  css: [
    { src: '~assets/materialize_mkti.scss', lang: 'scss' }
  ],

  head: {
    title: 'TP1 • Dielectric Vegetable Oil',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: 'TP1 DVO es un líquido aislante biodegradable con características iguales o mejores a los aceites dieléctricos minerales, para el uso de aparatos electrónicos y transformadores de distribución y potencia.' }
    ],
    script: [
      { src: "/js/materialize.js" }
      // { src: "/js/Backend.js" },
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' }
    ]
  },

  loading: {
    color: '#3B8070',
    height: '2px'
  },

  router: {
    middleware: 'i18n'
  },

  modules: [
    '@nuxtjs/axios',
    // ['@nuxtjs/google-tag-manager', { id: 'GTM-XXXXX', pageTracking: true }],
  ],

  plugins: [
    // { src: '~/plugins/gtag.js', ssr: false },
    // { src: '~/plugins/MiniBackend.js', ssr: false },
    { src: '~/plugins/i18n.js', ssr: true },
    { src: '~/plugins/eventBus.js', ssr: true },
    { src: '~/plugins/filters.js', ssr: false }
  ],

  env: {
    'backend_dir_dev': "http://localhost/cluster3/tp1/static/api",
    'backend_dir_gen': "http://tp1.com.mx/api"
  },

  build: {
    vendor: ['vue-i18n'],

    extend (config, { isDev, isClient }) {
      if (isDev && isClient) {
        config.module.rules.push({
          enforce: 'pre',
          test: /\.(js|vue)$/,
          loader: 'eslint-loader',
          options: { emitWarning: true },
          exclude: /(node_modules)/
        })
      }
    }
  }

}
