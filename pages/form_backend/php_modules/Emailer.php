<?php
class Emailer extends AbstractStatusObject{

    public function Send($from_name, $from_email, $to, $title, $MessageBody ){
      try{
        return @mail(
          $to,
          $title,
          $MessageBody,
          $this->getHeaders($from_name, $from_email),
          "-f $from_email"
        );
      }catch( Exception $e){
        $this->setError( "Error al enviar email." . $e->getMessage() );
        return False;
      }
    }

    public function getHeaders($from_name, $from_email){
      return
        "MIME-Version: 1.0\r\n".
        "From: $from_name <$from_email>\r\n".
        "Content-type: text/html; charset=utf-8\r\n".
        "X-Priority: 1\r\n".
        "Reply-To: $from_email\r\n".
        "X-Mailer: PHP/" . phpversion() . "";
    }
}
