<?php
  ini_set('default_charset', 'utf-8');
  header('Access-Control-Allow-Origin: *');
  header('Access-Control-Allow-Headers: *');

  error_reporting(E_ALL);
  ini_set('display_errors', 1);

  include( module('Utils') );
  include( module('Curl') );
  // include( module('Filer') );
  include( module('Router') );
  include( module('Templater') );

  include( module('LogError') );
  $Log = new LogError();

  include( module('Emailer') );
  $Emailer = new Emailer("contacto@tp1.com.mx", "TP1, Dielectric Vegetable Oil");

  // include( module('SQLite') );
  // include( module('SQLAPI') );
  // $SQL = new SQL();
  // $SQL->Connect2MySQL($db_name="unittests", $db_user="root", $db_password="root", $db_host="localhost");
  // $SQL->Connect2MySQL(
  //   $db_name="",
  //   $db_user="",
  //   $db_password="",
  //   $db_host=""
  // );

  // include( module('AppUser') );
  // $User = new AppUser($SQL);

  // include( module('Mailchimper') );
  // $Mailchimp = new Mailchimper("");

  // include( module('Slacker') );
  // $Slack = new Slacker();
  // $Slack->setToken("");

  function module($module_name){
    return ( __DIR__ . "/modules/$module_name.php");
  }
