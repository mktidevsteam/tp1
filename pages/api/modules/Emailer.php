<?php
class Emailer{
  public $from = "";
  public $name = null;
  public $headers = null;

  public function __construct($from, $name=null, $headers=null){
    $this->from = $from;
    $this->name = (is_null($name)) ? $from : $name;

    if( is_null($headers) ){
      $this->setDefaultHeaders($headers);
    }else{
      $this->setHeaders($headers);
    }

  }

  public function Send($to, $title, $message){
    $sent =
      mail(
        $to,
        $title,
        $message,
        $this->headers,
        ("-f ".$this->from)
      );

    return ($sent != false);
  }
  public function SendList($Tos, $title, $message){
    $all_sent = true;

    foreach($Tos as $to){
      $sent = $this->Send($to, $title, $message);
      if( ! $sent ){
        $all_sent = false;
      }
    }

    return $all_sent;
  }
  public function SendAssoc($ar){
    return $this->Send( $ar["to"], $ar["title"], $ar["message"] );
  }
  public function SendTable($Table, $title=null, $message=null){
    $all_sent = true;

    foreach($Table as $ar){
      $ar_title = (is_null($title)) ? $ar["title"] : $title;
      $ar_message = (is_null($message)) ? $ar["message"] : $message;
      $sent = $this->Send( $ar["to"], $ar_title, $ar_message);

      if( ! $sent ){
        $all_sent = false;
      }
    }

    return $all_sent;
  }

  public function setDefaultHeaders(){
    $this->setHeaders(
      "MIME-Version: 1.0\r\n".
      "From: ".$this->name." <".$this->from.">\r\n".
      "Content-type: text/html; charset=utf-8\r\n".
      "X-Priority: 1\r\n".
      "Reply-To: ".$this->from."\r\n".
      "X-Mailer: PHP/".phpversion().""
    );
  }
  public function setHeaders($headers){
    $this->headers = $headers;
  }

}
