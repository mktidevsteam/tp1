<?php

$GlobalCurler = new Curler();

class Curl {

  public static function GET($url, $vars = array()){
    return $GLOBALS["GlobalCurler"]->GET($url, $vars);
  }
  public static function POST($url, $vars = array()){
    return $GLOBALS["GlobalCurler"]->POST($url, $vars);
  }
  public static function PUT($url, $vars = array()){
    return $GLOBALS["GlobalCurler"]->PUT($url, $vars);
  }
  public static function DELETE($url, $vars = array()){
    return $GLOBALS["GlobalCurler"]->DELETE($url, $vars);
  }
  public static function HEAD($url, $vars = array()){
    return $GLOBALS["GlobalCurler"]->HEAD($url, $vars);
  }
  public static function PATCH($url, $vars = array()){
    return $GLOBALS["GlobalCurler"]->PATCH($url, $vars);
  }

  public static function REQUEST($method, $url, $vars = array()){
    return $GLOBALS["GlobalCurler"]->request($method, $url, $vars);
  }

}
class Curler{
    public $cookie_file;
    public $follow_redirects = true;
    public $headers = array();
    public $options = array();
    public $referer;
    public $user_agent;
    protected $request;

    public function __construct(){
      $this->cookie_file = ( dirname(__FILE__) . DIRECTORY_SEPARATOR . 'curl_cookie.txt' );
      $this->user_agent = isset($_SERVER['HTTP_USER_AGENT']) ? $_SERVER['HTTP_USER_AGENT'] : ('Curl/PHP '. PHP_VERSION);
    }

    public function GET($url, $vars = array()) {
      if ( ! empty($vars) ){
        $url .= (stripos($url, '?') !== false) ? '&' : '?';
        $url .= (is_string($vars)) ? $vars : http_build_query($vars, '', '&');
      }
      return $this->request('GET', $url);
    }
    public function POST($url, $vars = array()) {
      return $this->request('POST', $url, $vars);
    }
    public function PUT($url, $vars = array()) {
      return $this->request('PUT', $url, $vars);
    }
    public function DELETE($url, $vars = array()) {
      return $this->request('DELETE', $url, $vars);
    }
    public function HEAD($url, $vars = array()) {
      return $this->request('HEAD', $url, $vars);
    }
    public function PATCH($url, $vars = array()) {
      return $this->request('PATCH', $url, $vars);
    }

    public function request($method, $url, $vars = array()) {
      $this->request = curl_init();

      if (is_array($vars)){
        if ( !empty($vars["apikey"]) ){
          curl_setopt($this->request, CURLOPT_USERPWD, "apikey" . ":" . $vars["apikey"] );
          unset($vars["apikey"]);
        }

      }

      $this->set_request_method($method);
      $this->set_request_options($url, $vars, $method);
      $this->set_request_headers();

      $response = curl_exec($this->request);

      if ($response) {
        $response = new CurlResponse($response);
      } else {
        throw new Exception ("Curl error: " . curl_errno($this->request) . ' - ' . curl_error($this->request) );
      }

      curl_close($this->request);
      return $response;
    }

    protected function set_request_method($method) {
      switch (strtoupper($method)) {
        case 'HEAD':
            curl_setopt($this->request, CURLOPT_NOBODY, true);
            break;
        case 'GET':
            curl_setopt($this->request, CURLOPT_HTTPGET, true);
            break;
        case 'POST':
            curl_setopt($this->request, CURLOPT_POST, true);
            break;
        case 'PUT':
            curl_setopt($this->request, CURLOPT_CUSTOMREQUEST, "PUT");
            break;
        case 'PATCH':
            curl_setopt($this->request, CURLOPT_CUSTOMREQUEST, "PATCH");
            break;
        default:
            curl_setopt($this->request, CURLOPT_CUSTOMREQUEST, $method);
      }
    }
    protected function set_request_options($url, $vars, $method) {

      if($method=="GET"){
        $params = http_build_query($vars, '', '&');
        curl_setopt($this->request, CURLOPT_URL, ($url . "?". $params) );
      }else{
        curl_setopt($this->request, CURLOPT_URL, $url);
        $vars = json_encode($vars);
        if( ! empty($vars) ){
          curl_setopt($this->request, CURLOPT_POSTFIELDS, $vars );
        }
      }

      # Set some default CURL options
      curl_setopt($this->request, CURLOPT_HEADER, true);
      curl_setopt($this->request, CURLOPT_RETURNTRANSFER, true);
      curl_setopt($this->request, CURLOPT_USERAGENT, $this->user_agent);
      curl_setopt($this->request, CURLOPT_SSL_VERIFYPEER, 0);

      if ($this->cookie_file) {
        curl_setopt($this->request, CURLOPT_COOKIEFILE, $this->cookie_file);
        curl_setopt($this->request, CURLOPT_COOKIEJAR, $this->cookie_file);
      }
      if ($this->follow_redirects) curl_setopt($this->request, CURLOPT_FOLLOWLOCATION, true);
      if ($this->referer) curl_setopt($this->request, CURLOPT_REFERER, $this->referer);

      # Set any custom CURL options
      foreach ($this->options as $option => $value) {
        curl_setopt($this->request, constant('CURLOPT_'.str_replace('CURLOPT_', '', strtoupper($option) )), $value);
      }
    }
    protected function set_request_headers() {
      $headers = array();
      foreach ($this->headers as $key => $value) {
        $headers[] = "$key: $value";
      }
      curl_setopt($this->request, CURLOPT_HTTPHEADER, $headers);
      // curl_setopt($this->request, CURLOPT_HTTPHEADER, ["Content-Type: application/x-www-form-urlencoded"]);
    }
}
class CurlResponse {
    public $body = '';
    public $headers = array();

    public function __construct($response) {
      # Headers regex
      $pattern = '#HTTP/\d\.\d.*?$.*?\r\n\r\n#ims';

      # Extract headers from response
      preg_match_all($pattern, $response, $matches);
      $headers_string = array_pop($matches[0]);
      $headers = explode("\r\n", str_replace("\r\n\r\n", '', $headers_string));

      # Remove headers from the response body
      $this->body = str_replace($headers_string, '', $response);

      # Extract the version and status from the first header
      $version_and_status = array_shift($headers);
      preg_match('#HTTP/(\d\.\d)\s(\d\d\d)\s(.*)#', $version_and_status, $matches);
      $this->headers['Http-Version'] = $matches[1];
      $this->headers['Status-Code'] = $matches[2];
      $this->headers['Status'] = $matches[2].' '.$matches[3];

      # Convert headers into an associative array
      foreach ($headers as $header) {
        preg_match('#(.*?)\:\s(.*)#', $header, $matches);
        $this->headers[$matches[1]] = $matches[2];
      }
    }
    public function __toString() {
      return $this->body;
    }
}
