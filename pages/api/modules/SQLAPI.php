<?php
  class SQLAPI {
    public function Add($Values, $Params=array() ){
      $insert_id = null;
      $this->AlwaysAdd($Values);
      if( $this->BeforeAdd($Values) ){
        $ModifiedValues = $this->AtAdd($Values);
        $insert_id = $this->SQL->Insert( $this->table, $ModifiedValues, $Params);
        $this->AfterAdd($Values, $insert_id);
      }else{
        $this->FailAdd($Values);
      }

      $this->FinallyAdd($Values, $insert_id);
      return $insert_id;
    }
      public function AlwaysAdd($Values){}
      public function BeforeAdd($Values){return true;}
      public function AtAdd($Values){return $Values;}
      public function AfterAdd($Values, $insert_id){}
      public function FailAdd($Values){}
      public function FinallyAdd($Values, $insert_id=null){}

    public function Update($Values, $update_id=null, $Params=array()){
      $this->AlwaysUpdate($Values, $update_id);
      if( $this->BeforeUpdate($Values, $update_id) ){
        $ModifiedValues = $this->AtUpdate($Values, $update_id);
        $ModifiedParams = $this->setWhere($Params, $update_id);
        $this->SQL->Update( $this->table, $ModifiedValues, $ModifieParams);
        $this->AfterUpdate($Values, $update_id);
      }else{
        $this->FailUpdate($Values, $update_id);
      }

      $this->FinallyUpdate($Values, $update_id);
      return $update_id;
    }
      public function AlwaysUpdate($Values, $update_id=null){}
      public function BeforeUpdate($Values, $update_id=null){return true;}
      public function AtUpdate($Values, $update_id=null){return $Values;}
      public function AfterUpdate($Values, $update_id=null){}
      public function FailUpdate($Values, $update_id=null){}
      public function FinallyUpdate($Values, $update_id=null){}

    public function Delete($delete_id=null, $Params=array()){
      $this->AlwaysDelete($delete_id);
      if( $this->BeforeDelete($delete_id) ){
        $ModifiedParams = $this->setWhere($Params, $delete_id);
        $this->SQL->Delete( $this->table, $ModifieParams);
        $this->AfterDelete($delete_id);
      }else{
        $this->FailDelete($delete_id);
      }

      $this->FinallyDelete($delete_id);
      return $delete_id;
    }
      public function AlwaysDelete($delete_id=null){}
      public function BeforeDelete($delete_id=null){return true;}
      public function AfterDelete($delete_id=null){}
      public function FailDelete($delete_id=null){}
      public function FinallyDelete($delete_id=null){}

    public function setWhere($Params=array(), $id_value=null){
      if( ! is_null($id_value) ){
        if( sizeof($Params) == 0 ){
          $Params["where"] = [$this->id => $id_value];
        }else{
          if( ! has_key("where", $Params) ){
            $Params["where"] = [$this->id => $id_value];
          }
        }
      }

      return $Params;
    }

    public function Get($id_value){
      return $this->GetByField( $this->id, $id_value);
    }
    public function GetByField($field, $value){
      $Params["where"] = [
        "$field" => $value
      ];
      $DataTable = $this->SQL->Select( $this->table, $Params);

      if( sizeof($DataTable) > 0 ){
        return $DataTable[0];
      }else{
        return [];
      }
    }
    public function GetWhere($Where, $Params=array()){
      $Params["where"] = $Where;
      $DataTable = $this->SQL->Select( $this->table, $Params);
      return $DataTable;
    }
    public function GetAll($Params=array()){
      return $this->SQL->Select( $this->table, $Params=array() );
    }
    public function CountWhere($Where){
      $Params["where"] = $Where;
      return $this->SQL->Count( $this->table, $Params);
    }

    public function Exists($id_value){
      return $this->ExistsByField( $this->id, $id_value);
    }
    public function NotExists($id_value){
      return ( ! $this->Exists($id_value) );
    }
    public function ExistsByField($field, $value){
      $Params["where"] = [
        "$field" => $value
      ];
      return $this->SQL->Exists( $this->table, $Params);
    }
    public function NotExistsByField($field, $value){
      return ( ! $this->ExistsByField($field, $value) );
    }
    public function ExistsWhere($Where){
      $Params["where"] = $Where;
      return $this->SQL->Exists( $this->table, $Params);
    }
    public function NotExistsWhere($Where){
      return ( ! $this->ExistsWhere($Where) );
    }

    public function NOW($plus_hours=0){
      return $this->SQL->NOW($plus_hours);
    }
    public function getLastId(){
      return $this->SQL->getLastId();
    }
    public function getDescription(){
      return $this->SQL->describeTable( $this->table );
    }
    public function getFields(){
      $Table = $this->getDescription();
      $fields = [];
      foreach($Table as $row){
        $fields[] = $row["Field"];
      }
      return $fields;
    }
    public function Config($key, $value=null){
      return $this->SQL->Config($key, $value);
    }
    public function ConfigRead($key){
      return $this->SQL->ConfigRead($key);
    }
    public function ConfigWrite($key, $value){
      return $this->SQL->ConfigWrite($key, $value);
    }

    public function __construct(&$SQL){
      $this->SQL = $SQL;
    }
    public $table = "";
    public $id = "";
    public $SQL = null;
  }
