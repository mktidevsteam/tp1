<?php
  $Router->REQUEST("/api/workspace/all", function() use ($Workspace){
    $r = jheader();
    $r["data"] = $Workspace->GetAll();
    recho( $r );
  });

  $Router->REQUEST("/api/workspace/get", function($page_id) use ($Workspace){
    $r = jheader();
    $r["data"] = $Workspace->Get($page_id);
    recho( $r );
  });

  $Router->REQUEST("/api/workspace/fields", function() use ($Workspace){
    $Table = $Workspace->getDescription();
    echo DisplayTable($Table);
  });
